﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;

namespace SitioUno
{
    public partial class modulos 
    {
        int codigo;
        int value;
        public static string error;
        public  string [] array_key = new string [22];
        public static int i = 0;
        
        
       


        // -> MODULO UART
        public void uart()
        {
        }

        // -> MODULO MSR
        public string msr(int valor)
        {
            switch(valor)
            {
                case 0:
                    error = "Lectura Correcta";
                break;
                case 255:
                    error = "Tarjeta no detectada";
                break;
            }
            return error;
           
        }

        // -> MODULO ICC
        public int icc (int valor)
        {
            switch (valor)
            {
                case 0:         //EMV_OK
                    error = "Lectura Correcta";
                break;
                case -1:        //IC card reset failed.IC card reset failed.
                    error = "IC card reset failed.IC card reset failed.";
                break;
                case -2:        //IC card command failed.
                    error = "IC card command failed.";
                break;
                case -3:        //IC card has been blocked.
                    error = "IC card has been blocked.";
                break;
                case -5:        //There is no EMV  application has been blocked.
                    error = "There is no EMV  application has been blocked.";
                break;
                case -6:        //There is no EMV  application supported by terminal.
                    error = "There is no EMV  application supported by terminal.";
                break; 
                case -7:        //Transaction is canceled by user. 
                    error = "Transaction is canceled by user.";
                break;
                case -8:        //User operation timeout.
                    error = "User operation timeout.";
                break;
                case -9:        //IC card data format error.
                    error = "IC card data format error.";
                break;
                case -22:       //ICC response with 6985 when GAC or GPO.
                    error = "ICC response with 6985 when GAC or GPO.";
                break;
                case -24:       //File error.
                    error = "File error.";
                break;
                case 255:
                    error = "No se detecta tarjeta";
                break;
            }
            return value;
        }

        // -> MODULO RF
        public string rf (int valor)
        {
            switch (valor)
            {
                case 0:         //Success.
                    error = "Lectura Correcta";
                break;
                case 1:         //Parameter error.
                    error = "Error de Parámetro";
                break;
                case 2:         //RF module close.
                    error = "Cierre de Módulo";
                break;
                case 3:         //No specific card in sensing area.
                    error = "Tarjeta no detectada en el area de lectura";  
                break;
                case 4:         //Too much card in sensing area(communication conflict).
                    error = "Varias Tarjetas en el área de lectura";  
                break;
                case 6:         //Protocol error(The data response from card breaches the agreement).
                    error = "Error de Protocolo - La respuesta de datos desde la tarjeta incumple el acuerdo";
                break;
                case 19:        //Card not activated.
                    error = "Tarjeta no activada";
                break;
                case 20:        //Multi-card conflict.
                    error = "Conflicto Multitarjeta";
                break;
                case 21:        //No response timeout.
                    error = "Sin tiempo de espera de respuesta ";
                break;
                case 22:        //Protocol error.
                    error = "Error de Protocolo";
                break;
                case 23:        //Communication transmission error.
                    error = "Error de Comunicación (Transmisión)";
                break;
                case 24:        //M1 Card authentication failure.
                    error = "Autenticación Fallida - Tarjeta M1";
                break;
                case 25:        //Sector is not certified.
                    error = "Sector no certificado";
                break;
                case 26:        //The data format of value block is incorrect.
                    error = "El formato de dato de los valores de bloques es incorrecto";
                break;
                case 27:        //Card is still in sensing area.
                    error = "Tarjeta aún en el área de lectura";
                break;
                case 28:        //Card status error(If A/B card call M1 card interface, or M1 cardcall PiccIsoCommand interface)
                    error = "Error de estado de la Tarjeta";
                break;
                case 255:       //Interface chip does not exist or abnormal.
                    error = "Interface Chip - No Existe";
                break;
            }
            return error;
        }

       
        // -> MODULO KEYPAD
        public int  keypad(string valor)
        {
            array_key[i] = valor;
            int key_v = 0; 
            if (valor == "O")
            {
                key_v = 100;
            }
            else
            {
                key_v = 101;
            }
            i++;
            return key_v;
        }

        // -> MODULO PRINTER
        public string printer(int valor)
        {
            switch (valor)
            {
                case 0:             //Success
                    error = "Impresion exitosa.";
                break;
                case 1:             //Printer busy
                    error = "Impresora ocupada.";
                break;
                case 2:             //Out of paper
                    error = "Sin papel.";
                break;
                case 3:             //The format of print data packet error
                    error = "Error en el formato de impresión de error  paquetes de datos.";
                break;
                case 4:             //Printer problems
                    error = "Problemas de Impresora.";
                break;
                case 8:             //Printer over heating
                    error = "Impresora sobre calentada.";
                break;
                case 9:             //Printer voltage is too low
                    error = "Voltaje de la impresora es demasiado baja.";
                break;
                case 240:           //Print unfinished
                    error = "Imprimir sin terminar.";
                break;
                case 252:           //Lack of font
                    error = "Sin fuente.";
                break;
                case 254:           //Package too long
                    error = "Paquete demasiado largo.";
                break;
            }
            return error;
        }
    }
}
