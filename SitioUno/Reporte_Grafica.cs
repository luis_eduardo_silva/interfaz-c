﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace SitioUno
{
    class Report
    {
        
        public void archivo_pdf( SaveFileDialog sfd, TextBox textBox_modelo, TextBox textBox_serial)
        {
            sfd.Filter = "Pdf File |*.pdf";

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                // Creamos el documento con el tamaño de página tradicional
                Document doc = new Document(PageSize.LETTER);

                // Indicamos donde vamos a guardar el documento
                PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream(sfd.FileName, FileMode.Create));

                // Le colocamos el título y el autor
                // **Nota: Esto no será visible en el documento
                doc.AddTitle("Report PostTesting");
                doc.AddCreator("Site One Technology");
                doc.AddCreationDate();

                // Abrimos el archivo
                doc.Open();

                // Escribimos el encabezamiento en el documento
                doc.Add(new Paragraph("PostTesting"));
                doc.Add(Chunk.NEWLINE);


                iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 18, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

                // Creamos una tabla 
                PdfPTable tblTest = new PdfPTable(3);
                float[] values = new float[3];
                values[0] = 40;
                values[1] = 100;
                values[2] = 30;
      

                tblTest.SetWidths(values);
                tblTest.WidthPercentage = 70;
                
                tblTest.HorizontalAlignment = Element.ALIGN_CENTER;

                // Configuramos el título de las columnas de la tabla
                PdfPCell Module = new PdfPCell(new Phrase("Módulo"));
                Module.HorizontalAlignment = 1;
                Module.Border = 1;
                Module.BorderWidthBottom = 0;

                
            

                PdfPCell Status = new PdfPCell(new Phrase("Descripción"));
                Status.Border = 1;
                Status.HorizontalAlignment = 1;
                Status.BorderWidthBottom = 0;
                
               

                PdfPCell Description = new PdfPCell(new Phrase("Estado"));
                Description.Border= 1;
                Description.BorderWidthBottom = 0;
                Description.HorizontalAlignment = 1;
                

                // Añadimos las celdas a la tabla
                tblTest.AddCell(Module);
                tblTest.AddCell(Status);
                tblTest.AddCell(Description);
               

                // Llenamos la tabla con información
                tblTest.AddCell(new PdfPCell(new Paragraph("DIAL UP"))).HorizontalAlignment=0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph("GPRS"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("ICC"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("KEYPAD"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("LAN"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("MSR"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("TCP"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("RF"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("PRINTER"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph("WIFI"))).HorizontalAlignment = 0; ;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
                tblTest.AddCell(new PdfPCell(new Paragraph(""))).HorizontalAlignment = 0;
          

                //Imagen
                //Imagen
               iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance("sitiouno.jpg");
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_CENTER;
                float percentage = 0.0f;
                percentage = 150 / imagen.Width;
                imagen.ScalePercent(percentage * 200);

                // Insertamos la imagen en el documento
                doc.Add(imagen);
                doc.Add(Chunk.NEWLINE);
                doc.Add(Chunk.NEWLINE);

                doc.Add(new Paragraph("Modelo:   " + textBox_modelo.Text));
                doc.Add(Chunk.NEWLINE);
                doc.Add(new Paragraph("Serial:   " + textBox_serial.Text));
                doc.Add(Chunk.NEWLINE);
                doc.Add(new Paragraph("IMEI:     " ));
                doc.Add(Chunk.NEWLINE);
                doc.Add(Chunk.NEWLINE);
                doc.Add(Chunk.NEWLINE);


                // Finalmente, añadimos la tabla al documento PDF y cerramos el documento
                doc.Add(tblTest);

                doc.Close();

            }
        }
      
    }
}
