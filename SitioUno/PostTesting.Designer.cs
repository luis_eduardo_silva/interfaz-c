﻿namespace SitioUno
{
    partial class Form_PostTesting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        public void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_PostTesting));
            this.button_oppenp = new System.Windows.Forms.Button();
            this.port_name = new System.Windows.Forms.ComboBox();
            this.baud_rate = new System.Windows.Forms.ComboBox();
            this.button_closep = new System.Windows.Forms.Button();
            this.pos_serial = new System.IO.Ports.SerialPort(this.components);
            this.panel_communication = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label_databits = new System.Windows.Forms.Label();
            this.label_parity = new System.Windows.Forms.Label();
            this.data_bits = new System.Windows.Forms.ComboBox();
            this.parity = new System.Windows.Forms.ComboBox();
            this.label_baudrate = new System.Windows.Forms.Label();
            this.label_portname = new System.Windows.Forms.Label();
            this.tabControl_module = new System.Windows.Forms.TabControl();
            this.page_general = new System.Windows.Forms.TabPage();
            this.general = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label_printer = new System.Windows.Forms.Label();
            this.check_rf = new System.Windows.Forms.PictureBox();
            this.detail_rf = new System.Windows.Forms.LinkLabel();
            this.failed_rf = new System.Windows.Forms.PictureBox();
            this.label_rf = new System.Windows.Forms.Label();
            this.failed_wifi = new System.Windows.Forms.PictureBox();
            this.check_wifi = new System.Windows.Forms.PictureBox();
            this.failed_msr = new System.Windows.Forms.PictureBox();
            this.check_msr = new System.Windows.Forms.PictureBox();
            this.failed_lan = new System.Windows.Forms.PictureBox();
            this.check_lan = new System.Windows.Forms.PictureBox();
            this.failed_keypad = new System.Windows.Forms.PictureBox();
            this.check_keypad = new System.Windows.Forms.PictureBox();
            this.failed_icc = new System.Windows.Forms.PictureBox();
            this.check_icc = new System.Windows.Forms.PictureBox();
            this.failed_gprs = new System.Windows.Forms.PictureBox();
            this.failed_dial = new System.Windows.Forms.PictureBox();
            this.check_dial = new System.Windows.Forms.PictureBox();
            this.check_gprs = new System.Windows.Forms.PictureBox();
            this.detail_wifi = new System.Windows.Forms.LinkLabel();
            this.detail_printer = new System.Windows.Forms.LinkLabel();
            this.detail_msr = new System.Windows.Forms.LinkLabel();
            this.detail_lan = new System.Windows.Forms.LinkLabel();
            this.detail_keypad = new System.Windows.Forms.LinkLabel();
            this.detail_icc = new System.Windows.Forms.LinkLabel();
            this.detail_gprs = new System.Windows.Forms.LinkLabel();
            this.detail_dial = new System.Windows.Forms.LinkLabel();
            this.label_wifi = new System.Windows.Forms.Label();
            this.label_lan = new System.Windows.Forms.Label();
            this.label_gprs = new System.Windows.Forms.Label();
            this.label_dial = new System.Windows.Forms.Label();
            this.label_msr = new System.Windows.Forms.Label();
            this.label_keypad = new System.Windows.Forms.Label();
            this.label_icc = new System.Windows.Forms.Label();
            this.button_testg = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.page_comunicacion = new System.Windows.Forms.TabPage();
            this.panel_tcp = new System.Windows.Forms.Panel();
            this.textBox_recv_uart = new System.Windows.Forms.TextBox();
            this.buttonrecv_uart = new System.Windows.Forms.Button();
            this.textBox_send_uart = new System.Windows.Forms.TextBox();
            this.buttonsend_uart = new System.Windows.Forms.Button();
            this.label_tcp2 = new System.Windows.Forms.Label();
            this.panel_wifi = new System.Windows.Forms.Panel();
            this.textBox_wifi = new System.Windows.Forms.TextBox();
            this.label_wifi2 = new System.Windows.Forms.Label();
            this.buttontest_wifi = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_usb = new System.Windows.Forms.Panel();
            this.buttonrecv_usb = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox_usb = new System.Windows.Forms.TextBox();
            this.buttonsend_usb = new System.Windows.Forms.Button();
            this.label_usb2 = new System.Windows.Forms.Label();
            this.panel_lan = new System.Windows.Forms.Panel();
            this.textBox_lan = new System.Windows.Forms.TextBox();
            this.buttontest_lan = new System.Windows.Forms.Button();
            this.label_lan2 = new System.Windows.Forms.Label();
            this.panel_dial = new System.Windows.Forms.Panel();
            this.textBox_dial = new System.Windows.Forms.TextBox();
            this.buttontest_dial = new System.Windows.Forms.Button();
            this.label_dial2 = new System.Windows.Forms.Label();
            this.panel_gprs = new System.Windows.Forms.Panel();
            this.textBox_gprs = new System.Windows.Forms.TextBox();
            this.buttontest_gprs = new System.Windows.Forms.Button();
            this.label_gprs2 = new System.Windows.Forms.Label();
            this.page_keypad = new System.Windows.Forms.TabPage();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.buttontest_keypad = new System.Windows.Forms.Button();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.page_icc = new System.Windows.Forms.TabPage();
            this.label_icc2 = new System.Windows.Forms.Label();
            this.panel_icc = new System.Windows.Forms.Panel();
            this.textBox_icc = new System.Windows.Forms.TextBox();
            this.estado_icc = new System.Windows.Forms.Label();
            this.buttontest_icc = new System.Windows.Forms.Button();
            this.page_msr = new System.Windows.Forms.TabPage();
            this.label_msr2 = new System.Windows.Forms.Label();
            this.panel_msr = new System.Windows.Forms.Panel();
            this.estado_msr = new System.Windows.Forms.Label();
            this.textBox_msr = new System.Windows.Forms.TextBox();
            this.buttontest_msr = new System.Windows.Forms.Button();
            this.page_rf = new System.Windows.Forms.TabPage();
            this.label_rf2 = new System.Windows.Forms.Label();
            this.panel_rf = new System.Windows.Forms.Panel();
            this.estado_rf = new System.Windows.Forms.Label();
            this.textBox_rf = new System.Windows.Forms.TextBox();
            this.buttontest_rf = new System.Windows.Forms.Button();
            this.page_printer = new System.Windows.Forms.TabPage();
            this.label_printer2 = new System.Windows.Forms.Label();
            this.panel_printer = new System.Windows.Forms.Panel();
            this.estado_printer = new System.Windows.Forms.Label();
            this.textBox_printer = new System.Windows.Forms.TextBox();
            this.buttontest_printer = new System.Windows.Forms.Button();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.menu_archivo = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_itemreporte = new System.Windows.Forms.ToolStripMenuItem();
            this.item_guardarcomo = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.save_pdf = new System.Windows.Forms.SaveFileDialog();
            this.textBox_imei = new System.Windows.Forms.TextBox();
            this.textBox_modelo = new System.Windows.Forms.TextBox();
            this.textBox_serial = new System.Windows.Forms.TextBox();
            this.fecha = new System.Windows.Forms.Label();
            this.modelo = new System.Windows.Forms.Label();
            this.serial = new System.Windows.Forms.Label();
            this.imei = new System.Windows.Forms.Label();
            this.table_info = new System.Windows.Forms.TableLayoutPanel();
            this.textBox_hactual = new System.Windows.Forms.TextBox();
            this.textBox_hinicio = new System.Windows.Forms.TextBox();
            this.h_actual = new System.Windows.Forms.Label();
            this.h_inicio = new System.Windows.Forms.Label();
            this.textBox_fecha = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_tecnico = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.timer_session = new System.Windows.Forms.Timer(this.components);
            this.check_printer = new System.Windows.Forms.PictureBox();
            this.failed_printer = new System.Windows.Forms.PictureBox();
            this.panel_communication.SuspendLayout();
            this.tabControl_module.SuspendLayout();
            this.page_general.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_rf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_rf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_wifi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_wifi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_msr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_msr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_lan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_lan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_keypad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_keypad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_icc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_icc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_gprs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_dial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_dial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_gprs)).BeginInit();
            this.page_comunicacion.SuspendLayout();
            this.panel_tcp.SuspendLayout();
            this.panel_wifi.SuspendLayout();
            this.panel_usb.SuspendLayout();
            this.panel_lan.SuspendLayout();
            this.panel_dial.SuspendLayout();
            this.panel_gprs.SuspendLayout();
            this.page_keypad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            this.page_icc.SuspendLayout();
            this.panel_icc.SuspendLayout();
            this.page_msr.SuspendLayout();
            this.panel_msr.SuspendLayout();
            this.page_rf.SuspendLayout();
            this.panel_rf.SuspendLayout();
            this.page_printer.SuspendLayout();
            this.panel_printer.SuspendLayout();
            this.menu.SuspendLayout();
            this.table_info.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_printer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_printer)).BeginInit();
            this.SuspendLayout();
            // 
            // button_oppenp
            // 
            this.button_oppenp.Location = new System.Drawing.Point(17, 157);
            this.button_oppenp.Name = "button_oppenp";
            this.button_oppenp.Size = new System.Drawing.Size(63, 34);
            this.button_oppenp.TabIndex = 0;
            this.button_oppenp.Text = "Abrir Puerto";
            this.button_oppenp.UseVisualStyleBackColor = true;
            this.button_oppenp.Click += new System.EventHandler(this.button1_Click);
            // 
            // port_name
            // 
            this.port_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.port_name.FormattingEnabled = true;
            this.port_name.Location = new System.Drawing.Point(85, 41);
            this.port_name.Name = "port_name";
            this.port_name.Size = new System.Drawing.Size(61, 21);
            this.port_name.TabIndex = 1;
            this.port_name.SelectedIndexChanged += new System.EventHandler(this.port_name_SelectedIndexChanged);
            this.port_name.MouseClick += new System.Windows.Forms.MouseEventHandler(this.port_name_MouseClick);
            // 
            // baud_rate
            // 
            this.baud_rate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.baud_rate.FormattingEnabled = true;
            this.baud_rate.Items.AddRange(new object[] {
            "100",
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "115200"});
            this.baud_rate.Location = new System.Drawing.Point(85, 68);
            this.baud_rate.Name = "baud_rate";
            this.baud_rate.Size = new System.Drawing.Size(61, 21);
            this.baud_rate.TabIndex = 2;
            // 
            // button_closep
            // 
            this.button_closep.Location = new System.Drawing.Point(83, 157);
            this.button_closep.Name = "button_closep";
            this.button_closep.Size = new System.Drawing.Size(63, 34);
            this.button_closep.TabIndex = 3;
            this.button_closep.Text = "Cerrar Puerto";
            this.button_closep.UseVisualStyleBackColor = true;
            this.button_closep.Click += new System.EventHandler(this.button_closep_Click);
            // 
            // pos_serial
            // 
            this.pos_serial.DtrEnable = true;
            this.pos_serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.pos_serial_DataReceived);
            // 
            // panel_communication
            // 
            this.panel_communication.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_communication.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_communication.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_communication.Controls.Add(this.label2);
            this.panel_communication.Controls.Add(this.label_databits);
            this.panel_communication.Controls.Add(this.label_parity);
            this.panel_communication.Controls.Add(this.data_bits);
            this.panel_communication.Controls.Add(this.button_closep);
            this.panel_communication.Controls.Add(this.parity);
            this.panel_communication.Controls.Add(this.button_oppenp);
            this.panel_communication.Controls.Add(this.label_baudrate);
            this.panel_communication.Controls.Add(this.port_name);
            this.panel_communication.Controls.Add(this.baud_rate);
            this.panel_communication.Controls.Add(this.label_portname);
            this.panel_communication.Location = new System.Drawing.Point(31, 245);
            this.panel_communication.Name = "panel_communication";
            this.panel_communication.Size = new System.Drawing.Size(165, 220);
            this.panel_communication.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Serial";
            // 
            // label_databits
            // 
            this.label_databits.AutoSize = true;
            this.label_databits.Location = new System.Drawing.Point(25, 125);
            this.label_databits.Name = "label_databits";
            this.label_databits.Size = new System.Drawing.Size(47, 13);
            this.label_databits.TabIndex = 5;
            this.label_databits.Text = "DataBits";
            // 
            // label_parity
            // 
            this.label_parity.AutoSize = true;
            this.label_parity.Location = new System.Drawing.Point(25, 98);
            this.label_parity.Name = "label_parity";
            this.label_parity.Size = new System.Drawing.Size(33, 13);
            this.label_parity.TabIndex = 4;
            this.label_parity.Text = "Parity";
            // 
            // data_bits
            // 
            this.data_bits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.data_bits.FormattingEnabled = true;
            this.data_bits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.data_bits.Location = new System.Drawing.Point(85, 122);
            this.data_bits.Name = "data_bits";
            this.data_bits.Size = new System.Drawing.Size(61, 21);
            this.data_bits.TabIndex = 3;
            // 
            // parity
            // 
            this.parity.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.parity.FormattingEnabled = true;
            this.parity.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.parity.Location = new System.Drawing.Point(85, 95);
            this.parity.Name = "parity";
            this.parity.Size = new System.Drawing.Size(61, 21);
            this.parity.TabIndex = 2;
            // 
            // label_baudrate
            // 
            this.label_baudrate.AutoSize = true;
            this.label_baudrate.Location = new System.Drawing.Point(21, 71);
            this.label_baudrate.Name = "label_baudrate";
            this.label_baudrate.Size = new System.Drawing.Size(58, 13);
            this.label_baudrate.TabIndex = 1;
            this.label_baudrate.Text = "Baud Rate";
            // 
            // label_portname
            // 
            this.label_portname.AutoSize = true;
            this.label_portname.Location = new System.Drawing.Point(21, 44);
            this.label_portname.Name = "label_portname";
            this.label_portname.Size = new System.Drawing.Size(57, 13);
            this.label_portname.TabIndex = 0;
            this.label_portname.Text = "Port Name";
            // 
            // tabControl_module
            // 
            this.tabControl_module.Controls.Add(this.page_general);
            this.tabControl_module.Controls.Add(this.page_comunicacion);
            this.tabControl_module.Controls.Add(this.page_keypad);
            this.tabControl_module.Controls.Add(this.tabPage8);
            this.tabControl_module.Controls.Add(this.page_icc);
            this.tabControl_module.Controls.Add(this.page_msr);
            this.tabControl_module.Controls.Add(this.page_rf);
            this.tabControl_module.Controls.Add(this.page_printer);
            this.tabControl_module.Location = new System.Drawing.Point(265, 89);
            this.tabControl_module.Name = "tabControl_module";
            this.tabControl_module.SelectedIndex = 0;
            this.tabControl_module.Size = new System.Drawing.Size(460, 480);
            this.tabControl_module.TabIndex = 10;
            // 
            // page_general
            // 
            this.page_general.BackColor = System.Drawing.SystemColors.Control;
            this.page_general.Controls.Add(this.general);
            this.page_general.Controls.Add(this.panel11);
            this.page_general.Controls.Add(this.button_testg);
            this.page_general.Controls.Add(this.label7);
            this.page_general.Controls.Add(this.progressBar1);
            this.page_general.Location = new System.Drawing.Point(4, 22);
            this.page_general.Name = "page_general";
            this.page_general.Size = new System.Drawing.Size(452, 454);
            this.page_general.TabIndex = 2;
            this.page_general.Text = "General";
            // 
            // general
            // 
            this.general.AutoSize = true;
            this.general.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.general.Location = new System.Drawing.Point(156, 35);
            this.general.Name = "general";
            this.general.Size = new System.Drawing.Size(146, 20);
            this.general.TabIndex = 54;
            this.general.Text = "Revisión General";
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel11.Controls.Add(this.failed_printer);
            this.panel11.Controls.Add(this.check_printer);
            this.panel11.Controls.Add(this.label_printer);
            this.panel11.Controls.Add(this.check_rf);
            this.panel11.Controls.Add(this.detail_rf);
            this.panel11.Controls.Add(this.failed_rf);
            this.panel11.Controls.Add(this.label_rf);
            this.panel11.Controls.Add(this.failed_wifi);
            this.panel11.Controls.Add(this.check_wifi);
            this.panel11.Controls.Add(this.failed_msr);
            this.panel11.Controls.Add(this.check_msr);
            this.panel11.Controls.Add(this.failed_lan);
            this.panel11.Controls.Add(this.check_lan);
            this.panel11.Controls.Add(this.failed_keypad);
            this.panel11.Controls.Add(this.check_keypad);
            this.panel11.Controls.Add(this.failed_icc);
            this.panel11.Controls.Add(this.check_icc);
            this.panel11.Controls.Add(this.failed_gprs);
            this.panel11.Controls.Add(this.failed_dial);
            this.panel11.Controls.Add(this.check_dial);
            this.panel11.Controls.Add(this.check_gprs);
            this.panel11.Controls.Add(this.detail_wifi);
            this.panel11.Controls.Add(this.detail_printer);
            this.panel11.Controls.Add(this.detail_msr);
            this.panel11.Controls.Add(this.detail_lan);
            this.panel11.Controls.Add(this.detail_keypad);
            this.panel11.Controls.Add(this.detail_icc);
            this.panel11.Controls.Add(this.detail_gprs);
            this.panel11.Controls.Add(this.detail_dial);
            this.panel11.Controls.Add(this.label_wifi);
            this.panel11.Controls.Add(this.label_lan);
            this.panel11.Controls.Add(this.label_gprs);
            this.panel11.Controls.Add(this.label_dial);
            this.panel11.Controls.Add(this.label_msr);
            this.panel11.Controls.Add(this.label_keypad);
            this.panel11.Controls.Add(this.label_icc);
            this.panel11.Location = new System.Drawing.Point(57, 98);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(341, 228);
            this.panel11.TabIndex = 53;
            // 
            // label_printer
            // 
            this.label_printer.AutoSize = true;
            this.label_printer.Location = new System.Drawing.Point(16, 35);
            this.label_printer.Name = "label_printer";
            this.label_printer.Size = new System.Drawing.Size(55, 13);
            this.label_printer.TabIndex = 88;
            this.label_printer.Text = "PRINTER";
            // 
            // check_rf
            // 
            this.check_rf.Image = ((System.Drawing.Image)(resources.GetObject("check_rf.Image")));
            this.check_rf.Location = new System.Drawing.Point(255, 11);
            this.check_rf.Name = "check_rf";
            this.check_rf.Size = new System.Drawing.Size(16, 13);
            this.check_rf.TabIndex = 87;
            this.check_rf.TabStop = false;
            // 
            // detail_rf
            // 
            this.detail_rf.AutoSize = true;
            this.detail_rf.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_rf.Location = new System.Drawing.Point(291, 11);
            this.detail_rf.Name = "detail_rf";
            this.detail_rf.Size = new System.Drawing.Size(45, 13);
            this.detail_rf.TabIndex = 86;
            this.detail_rf.TabStop = true;
            this.detail_rf.Text = "Detalles";
            this.detail_rf.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.detailrf_LinkClicked);
            // 
            // failed_rf
            // 
            this.failed_rf.Image = ((System.Drawing.Image)(resources.GetObject("failed_rf.Image")));
            this.failed_rf.Location = new System.Drawing.Point(255, 10);
            this.failed_rf.Name = "failed_rf";
            this.failed_rf.Size = new System.Drawing.Size(16, 16);
            this.failed_rf.TabIndex = 85;
            this.failed_rf.TabStop = false;
            // 
            // label_rf
            // 
            this.label_rf.AutoSize = true;
            this.label_rf.Location = new System.Drawing.Point(17, 11);
            this.label_rf.Name = "label_rf";
            this.label_rf.Size = new System.Drawing.Size(21, 13);
            this.label_rf.TabIndex = 84;
            this.label_rf.Text = "RF";
            // 
            // failed_wifi
            // 
            this.failed_wifi.Image = ((System.Drawing.Image)(resources.GetObject("failed_wifi.Image")));
            this.failed_wifi.Location = new System.Drawing.Point(255, 203);
            this.failed_wifi.Name = "failed_wifi";
            this.failed_wifi.Size = new System.Drawing.Size(16, 16);
            this.failed_wifi.TabIndex = 83;
            this.failed_wifi.TabStop = false;
            // 
            // check_wifi
            // 
            this.check_wifi.Image = ((System.Drawing.Image)(resources.GetObject("check_wifi.Image")));
            this.check_wifi.Location = new System.Drawing.Point(255, 203);
            this.check_wifi.Name = "check_wifi";
            this.check_wifi.Size = new System.Drawing.Size(16, 13);
            this.check_wifi.TabIndex = 82;
            this.check_wifi.TabStop = false;
            // 
            // failed_msr
            // 
            this.failed_msr.Image = ((System.Drawing.Image)(resources.GetObject("failed_msr.Image")));
            this.failed_msr.Location = new System.Drawing.Point(255, 107);
            this.failed_msr.Name = "failed_msr";
            this.failed_msr.Size = new System.Drawing.Size(16, 16);
            this.failed_msr.TabIndex = 79;
            this.failed_msr.TabStop = false;
            // 
            // check_msr
            // 
            this.check_msr.Image = ((System.Drawing.Image)(resources.GetObject("check_msr.Image")));
            this.check_msr.Location = new System.Drawing.Point(255, 107);
            this.check_msr.Name = "check_msr";
            this.check_msr.Size = new System.Drawing.Size(16, 13);
            this.check_msr.TabIndex = 78;
            this.check_msr.TabStop = false;
            // 
            // failed_lan
            // 
            this.failed_lan.Image = ((System.Drawing.Image)(resources.GetObject("failed_lan.Image")));
            this.failed_lan.Location = new System.Drawing.Point(255, 131);
            this.failed_lan.Name = "failed_lan";
            this.failed_lan.Size = new System.Drawing.Size(16, 16);
            this.failed_lan.TabIndex = 77;
            this.failed_lan.TabStop = false;
            // 
            // check_lan
            // 
            this.check_lan.Image = ((System.Drawing.Image)(resources.GetObject("check_lan.Image")));
            this.check_lan.Location = new System.Drawing.Point(255, 131);
            this.check_lan.Name = "check_lan";
            this.check_lan.Size = new System.Drawing.Size(16, 13);
            this.check_lan.TabIndex = 76;
            this.check_lan.TabStop = false;
            // 
            // failed_keypad
            // 
            this.failed_keypad.Image = ((System.Drawing.Image)(resources.GetObject("failed_keypad.Image")));
            this.failed_keypad.Location = new System.Drawing.Point(255, 80);
            this.failed_keypad.Name = "failed_keypad";
            this.failed_keypad.Size = new System.Drawing.Size(16, 16);
            this.failed_keypad.TabIndex = 75;
            this.failed_keypad.TabStop = false;
            // 
            // check_keypad
            // 
            this.check_keypad.Image = ((System.Drawing.Image)(resources.GetObject("check_keypad.Image")));
            this.check_keypad.Location = new System.Drawing.Point(255, 83);
            this.check_keypad.Name = "check_keypad";
            this.check_keypad.Size = new System.Drawing.Size(16, 13);
            this.check_keypad.TabIndex = 74;
            this.check_keypad.TabStop = false;
            // 
            // failed_icc
            // 
            this.failed_icc.Image = ((System.Drawing.Image)(resources.GetObject("failed_icc.Image")));
            this.failed_icc.Location = new System.Drawing.Point(255, 54);
            this.failed_icc.Name = "failed_icc";
            this.failed_icc.Size = new System.Drawing.Size(16, 10);
            this.failed_icc.TabIndex = 73;
            this.failed_icc.TabStop = false;
            // 
            // check_icc
            // 
            this.check_icc.Image = ((System.Drawing.Image)(resources.GetObject("check_icc.Image")));
            this.check_icc.Location = new System.Drawing.Point(255, 54);
            this.check_icc.Name = "check_icc";
            this.check_icc.Size = new System.Drawing.Size(16, 13);
            this.check_icc.TabIndex = 72;
            this.check_icc.TabStop = false;
            // 
            // failed_gprs
            // 
            this.failed_gprs.Image = ((System.Drawing.Image)(resources.GetObject("failed_gprs.Image")));
            this.failed_gprs.Location = new System.Drawing.Point(255, 179);
            this.failed_gprs.Name = "failed_gprs";
            this.failed_gprs.Size = new System.Drawing.Size(16, 16);
            this.failed_gprs.TabIndex = 71;
            this.failed_gprs.TabStop = false;
            // 
            // failed_dial
            // 
            this.failed_dial.Image = ((System.Drawing.Image)(resources.GetObject("failed_dial.Image")));
            this.failed_dial.Location = new System.Drawing.Point(255, 156);
            this.failed_dial.Name = "failed_dial";
            this.failed_dial.Size = new System.Drawing.Size(16, 14);
            this.failed_dial.TabIndex = 70;
            this.failed_dial.TabStop = false;
            // 
            // check_dial
            // 
            this.check_dial.Image = ((System.Drawing.Image)(resources.GetObject("check_dial.Image")));
            this.check_dial.Location = new System.Drawing.Point(255, 156);
            this.check_dial.Name = "check_dial";
            this.check_dial.Size = new System.Drawing.Size(16, 13);
            this.check_dial.TabIndex = 69;
            this.check_dial.TabStop = false;
            // 
            // check_gprs
            // 
            this.check_gprs.Image = ((System.Drawing.Image)(resources.GetObject("check_gprs.Image")));
            this.check_gprs.Location = new System.Drawing.Point(255, 179);
            this.check_gprs.Name = "check_gprs";
            this.check_gprs.Size = new System.Drawing.Size(16, 13);
            this.check_gprs.TabIndex = 68;
            this.check_gprs.TabStop = false;
            // 
            // detail_wifi
            // 
            this.detail_wifi.AutoSize = true;
            this.detail_wifi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_wifi.Location = new System.Drawing.Point(291, 203);
            this.detail_wifi.Name = "detail_wifi";
            this.detail_wifi.Size = new System.Drawing.Size(45, 13);
            this.detail_wifi.TabIndex = 67;
            this.detail_wifi.TabStop = true;
            this.detail_wifi.Text = "Detalles";
            // 
            // detail_printer
            // 
            this.detail_printer.AutoSize = true;
            this.detail_printer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_printer.Location = new System.Drawing.Point(291, 35);
            this.detail_printer.Name = "detail_printer";
            this.detail_printer.Size = new System.Drawing.Size(45, 13);
            this.detail_printer.TabIndex = 66;
            this.detail_printer.TabStop = true;
            this.detail_printer.Text = "Detalles";
            // 
            // detail_msr
            // 
            this.detail_msr.AutoSize = true;
            this.detail_msr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_msr.Location = new System.Drawing.Point(291, 107);
            this.detail_msr.Name = "detail_msr";
            this.detail_msr.Size = new System.Drawing.Size(45, 13);
            this.detail_msr.TabIndex = 65;
            this.detail_msr.TabStop = true;
            this.detail_msr.Text = "Detalles";
            this.detail_msr.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.detailrf_LinkClicked);
            // 
            // detail_lan
            // 
            this.detail_lan.AutoSize = true;
            this.detail_lan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_lan.Location = new System.Drawing.Point(291, 131);
            this.detail_lan.Name = "detail_lan";
            this.detail_lan.Size = new System.Drawing.Size(45, 13);
            this.detail_lan.TabIndex = 64;
            this.detail_lan.TabStop = true;
            this.detail_lan.Text = "Detalles";
            // 
            // detail_keypad
            // 
            this.detail_keypad.AutoSize = true;
            this.detail_keypad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_keypad.Location = new System.Drawing.Point(291, 83);
            this.detail_keypad.Name = "detail_keypad";
            this.detail_keypad.Size = new System.Drawing.Size(45, 13);
            this.detail_keypad.TabIndex = 63;
            this.detail_keypad.TabStop = true;
            this.detail_keypad.Text = "Detalles";
            this.detail_keypad.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.detailkeypad_LinkClicked_1);
            // 
            // detail_icc
            // 
            this.detail_icc.AutoSize = true;
            this.detail_icc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_icc.Location = new System.Drawing.Point(291, 59);
            this.detail_icc.Name = "detail_icc";
            this.detail_icc.Size = new System.Drawing.Size(45, 13);
            this.detail_icc.TabIndex = 62;
            this.detail_icc.TabStop = true;
            this.detail_icc.Text = "Detalles";
            this.detail_icc.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.detailicc_LinkClicked);
            // 
            // detail_gprs
            // 
            this.detail_gprs.AutoSize = true;
            this.detail_gprs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_gprs.Location = new System.Drawing.Point(291, 179);
            this.detail_gprs.Name = "detail_gprs";
            this.detail_gprs.Size = new System.Drawing.Size(45, 13);
            this.detail_gprs.TabIndex = 61;
            this.detail_gprs.TabStop = true;
            this.detail_gprs.Text = "Detalles";
            // 
            // detail_dial
            // 
            this.detail_dial.AutoSize = true;
            this.detail_dial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.detail_dial.Location = new System.Drawing.Point(291, 155);
            this.detail_dial.Name = "detail_dial";
            this.detail_dial.Size = new System.Drawing.Size(45, 13);
            this.detail_dial.TabIndex = 60;
            this.detail_dial.TabStop = true;
            this.detail_dial.Text = "Detalles";
            // 
            // label_wifi
            // 
            this.label_wifi.AutoSize = true;
            this.label_wifi.Location = new System.Drawing.Point(17, 203);
            this.label_wifi.Name = "label_wifi";
            this.label_wifi.Size = new System.Drawing.Size(30, 13);
            this.label_wifi.TabIndex = 59;
            this.label_wifi.Text = "WIFI";
            // 
            // label_lan
            // 
            this.label_lan.AutoSize = true;
            this.label_lan.Location = new System.Drawing.Point(16, 131);
            this.label_lan.Name = "label_lan";
            this.label_lan.Size = new System.Drawing.Size(28, 13);
            this.label_lan.TabIndex = 57;
            this.label_lan.Text = "LAN";
            // 
            // label_gprs
            // 
            this.label_gprs.AutoSize = true;
            this.label_gprs.Location = new System.Drawing.Point(17, 179);
            this.label_gprs.Name = "label_gprs";
            this.label_gprs.Size = new System.Drawing.Size(37, 13);
            this.label_gprs.TabIndex = 56;
            this.label_gprs.Text = "GPRS";
            // 
            // label_dial
            // 
            this.label_dial.AutoSize = true;
            this.label_dial.Location = new System.Drawing.Point(17, 155);
            this.label_dial.Name = "label_dial";
            this.label_dial.Size = new System.Drawing.Size(49, 13);
            this.label_dial.TabIndex = 55;
            this.label_dial.Text = "DIAL UP";
            // 
            // label_msr
            // 
            this.label_msr.AutoSize = true;
            this.label_msr.Location = new System.Drawing.Point(16, 107);
            this.label_msr.Name = "label_msr";
            this.label_msr.Size = new System.Drawing.Size(31, 13);
            this.label_msr.TabIndex = 54;
            this.label_msr.Text = "MSR";
            // 
            // label_keypad
            // 
            this.label_keypad.AutoSize = true;
            this.label_keypad.Location = new System.Drawing.Point(17, 83);
            this.label_keypad.Name = "label_keypad";
            this.label_keypad.Size = new System.Drawing.Size(50, 13);
            this.label_keypad.TabIndex = 53;
            this.label_keypad.Text = "KEYPAD";
            // 
            // label_icc
            // 
            this.label_icc.AutoSize = true;
            this.label_icc.Location = new System.Drawing.Point(16, 59);
            this.label_icc.Name = "label_icc";
            this.label_icc.Size = new System.Drawing.Size(24, 13);
            this.label_icc.TabIndex = 52;
            this.label_icc.Text = "ICC";
            // 
            // button_testg
            // 
            this.button_testg.Enabled = false;
            this.button_testg.Location = new System.Drawing.Point(160, 332);
            this.button_testg.Name = "button_testg";
            this.button_testg.Size = new System.Drawing.Size(154, 42);
            this.button_testg.TabIndex = 52;
            this.button_testg.Text = "TEST\r\nGENERAL\r\n";
            this.button_testg.UseVisualStyleBackColor = true;
            this.button_testg.Click += new System.EventHandler(this.button_testg_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 5;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(57, 66);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(341, 21);
            this.progressBar1.TabIndex = 4;
            // 
            // page_comunicacion
            // 
            this.page_comunicacion.BackColor = System.Drawing.SystemColors.Control;
            this.page_comunicacion.Controls.Add(this.panel_tcp);
            this.page_comunicacion.Controls.Add(this.panel_wifi);
            this.page_comunicacion.Controls.Add(this.label3);
            this.page_comunicacion.Controls.Add(this.panel_usb);
            this.page_comunicacion.Controls.Add(this.panel_lan);
            this.page_comunicacion.Controls.Add(this.panel_dial);
            this.page_comunicacion.Controls.Add(this.panel_gprs);
            this.page_comunicacion.Location = new System.Drawing.Point(4, 22);
            this.page_comunicacion.Name = "page_comunicacion";
            this.page_comunicacion.Padding = new System.Windows.Forms.Padding(3);
            this.page_comunicacion.Size = new System.Drawing.Size(452, 454);
            this.page_comunicacion.TabIndex = 0;
            this.page_comunicacion.Text = "Comunicación";
            // 
            // panel_tcp
            // 
            this.panel_tcp.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_tcp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_tcp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_tcp.Controls.Add(this.textBox_recv_uart);
            this.panel_tcp.Controls.Add(this.buttonrecv_uart);
            this.panel_tcp.Controls.Add(this.textBox_send_uart);
            this.panel_tcp.Controls.Add(this.buttonsend_uart);
            this.panel_tcp.Controls.Add(this.label_tcp2);
            this.panel_tcp.Enabled = false;
            this.panel_tcp.Location = new System.Drawing.Point(230, 309);
            this.panel_tcp.Name = "panel_tcp";
            this.panel_tcp.Size = new System.Drawing.Size(185, 115);
            this.panel_tcp.TabIndex = 8;
            // 
            // textBox_recv_uart
            // 
            this.textBox_recv_uart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_recv_uart.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox_recv_uart.Location = new System.Drawing.Point(14, 48);
            this.textBox_recv_uart.Multiline = true;
            this.textBox_recv_uart.Name = "textBox_recv_uart";
            this.textBox_recv_uart.Size = new System.Drawing.Size(157, 33);
            this.textBox_recv_uart.TabIndex = 22;
            // 
            // buttonrecv_uart
            // 
            this.buttonrecv_uart.Location = new System.Drawing.Point(95, 87);
            this.buttonrecv_uart.Name = "buttonrecv_uart";
            this.buttonrecv_uart.Size = new System.Drawing.Size(59, 23);
            this.buttonrecv_uart.TabIndex = 21;
            this.buttonrecv_uart.Text = "Recibir";
            this.buttonrecv_uart.UseVisualStyleBackColor = true;
            // 
            // textBox_send_uart
            // 
            this.textBox_send_uart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_send_uart.Location = new System.Drawing.Point(14, 27);
            this.textBox_send_uart.Multiline = true;
            this.textBox_send_uart.Name = "textBox_send_uart";
            this.textBox_send_uart.Size = new System.Drawing.Size(157, 17);
            this.textBox_send_uart.TabIndex = 20;
            // 
            // buttonsend_uart
            // 
            this.buttonsend_uart.Location = new System.Drawing.Point(30, 87);
            this.buttonsend_uart.Name = "buttonsend_uart";
            this.buttonsend_uart.Size = new System.Drawing.Size(59, 23);
            this.buttonsend_uart.TabIndex = 18;
            this.buttonsend_uart.Text = "Enviar";
            this.buttonsend_uart.UseVisualStyleBackColor = true;
            this.buttonsend_uart.Click += new System.EventHandler(this.buttonsend_uart_Click);
            // 
            // label_tcp2
            // 
            this.label_tcp2.AutoSize = true;
            this.label_tcp2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_tcp2.Location = new System.Drawing.Point(76, 9);
            this.label_tcp2.Name = "label_tcp2";
            this.label_tcp2.Size = new System.Drawing.Size(43, 15);
            this.label_tcp2.TabIndex = 15;
            this.label_tcp2.Text = "UART";
            // 
            // panel_wifi
            // 
            this.panel_wifi.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_wifi.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_wifi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_wifi.Controls.Add(this.textBox_wifi);
            this.panel_wifi.Controls.Add(this.label_wifi2);
            this.panel_wifi.Controls.Add(this.buttontest_wifi);
            this.panel_wifi.Location = new System.Drawing.Point(230, 188);
            this.panel_wifi.Name = "panel_wifi";
            this.panel_wifi.Size = new System.Drawing.Size(185, 116);
            this.panel_wifi.TabIndex = 11;
            // 
            // textBox_wifi
            // 
            this.textBox_wifi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_wifi.Location = new System.Drawing.Point(14, 27);
            this.textBox_wifi.Multiline = true;
            this.textBox_wifi.Name = "textBox_wifi";
            this.textBox_wifi.Size = new System.Drawing.Size(157, 44);
            this.textBox_wifi.TabIndex = 20;
            // 
            // label_wifi2
            // 
            this.label_wifi2.AutoSize = true;
            this.label_wifi2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_wifi2.Location = new System.Drawing.Point(76, 9);
            this.label_wifi2.Name = "label_wifi2";
            this.label_wifi2.Size = new System.Drawing.Size(35, 15);
            this.label_wifi2.TabIndex = 17;
            this.label_wifi2.Text = "WIFI";
            // 
            // buttontest_wifi
            // 
            this.buttontest_wifi.Location = new System.Drawing.Point(63, 74);
            this.buttontest_wifi.Name = "buttontest_wifi";
            this.buttontest_wifi.Size = new System.Drawing.Size(59, 23);
            this.buttontest_wifi.TabIndex = 18;
            this.buttontest_wifi.Text = "Test";
            this.buttontest_wifi.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(116, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 20);
            this.label3.TabIndex = 22;
            this.label3.Text = "Módulos de Comunicación";
            // 
            // panel_usb
            // 
            this.panel_usb.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_usb.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_usb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_usb.Controls.Add(this.buttonrecv_usb);
            this.panel_usb.Controls.Add(this.textBox1);
            this.panel_usb.Controls.Add(this.textBox_usb);
            this.panel_usb.Controls.Add(this.buttonsend_usb);
            this.panel_usb.Controls.Add(this.label_usb2);
            this.panel_usb.Enabled = false;
            this.panel_usb.Location = new System.Drawing.Point(37, 309);
            this.panel_usb.Name = "panel_usb";
            this.panel_usb.Size = new System.Drawing.Size(185, 116);
            this.panel_usb.TabIndex = 10;
            // 
            // buttonrecv_usb
            // 
            this.buttonrecv_usb.Location = new System.Drawing.Point(95, 87);
            this.buttonrecv_usb.Name = "buttonrecv_usb";
            this.buttonrecv_usb.Size = new System.Drawing.Size(59, 23);
            this.buttonrecv_usb.TabIndex = 23;
            this.buttonrecv_usb.Text = "Recibir";
            this.buttonrecv_usb.UseVisualStyleBackColor = true;
            this.buttonrecv_usb.Click += new System.EventHandler(this.buttonrecv_usb_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox1.Location = new System.Drawing.Point(14, 48);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(157, 33);
            this.textBox1.TabIndex = 23;
            // 
            // textBox_usb
            // 
            this.textBox_usb.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_usb.Location = new System.Drawing.Point(14, 27);
            this.textBox_usb.Multiline = true;
            this.textBox_usb.Name = "textBox_usb";
            this.textBox_usb.Size = new System.Drawing.Size(157, 17);
            this.textBox_usb.TabIndex = 19;
            // 
            // buttonsend_usb
            // 
            this.buttonsend_usb.Location = new System.Drawing.Point(30, 87);
            this.buttonsend_usb.Name = "buttonsend_usb";
            this.buttonsend_usb.Size = new System.Drawing.Size(59, 23);
            this.buttonsend_usb.TabIndex = 18;
            this.buttonsend_usb.Text = "Enviar";
            this.buttonsend_usb.UseVisualStyleBackColor = true;
            // 
            // label_usb2
            // 
            this.label_usb2.AutoSize = true;
            this.label_usb2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_usb2.Location = new System.Drawing.Point(76, 9);
            this.label_usb2.Name = "label_usb2";
            this.label_usb2.Size = new System.Drawing.Size(35, 15);
            this.label_usb2.TabIndex = 16;
            this.label_usb2.Text = "USB";
            // 
            // panel_lan
            // 
            this.panel_lan.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_lan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_lan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_lan.Controls.Add(this.textBox_lan);
            this.panel_lan.Controls.Add(this.buttontest_lan);
            this.panel_lan.Controls.Add(this.label_lan2);
            this.panel_lan.Location = new System.Drawing.Point(37, 188);
            this.panel_lan.Name = "panel_lan";
            this.panel_lan.Size = new System.Drawing.Size(185, 115);
            this.panel_lan.TabIndex = 9;
            // 
            // textBox_lan
            // 
            this.textBox_lan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_lan.Location = new System.Drawing.Point(14, 27);
            this.textBox_lan.Multiline = true;
            this.textBox_lan.Name = "textBox_lan";
            this.textBox_lan.Size = new System.Drawing.Size(157, 44);
            this.textBox_lan.TabIndex = 19;
            // 
            // buttontest_lan
            // 
            this.buttontest_lan.Location = new System.Drawing.Point(66, 77);
            this.buttontest_lan.Name = "buttontest_lan";
            this.buttontest_lan.Size = new System.Drawing.Size(59, 23);
            this.buttontest_lan.TabIndex = 18;
            this.buttontest_lan.Text = "Test";
            this.buttontest_lan.UseVisualStyleBackColor = true;
            // 
            // label_lan2
            // 
            this.label_lan2.AutoSize = true;
            this.label_lan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_lan2.Location = new System.Drawing.Point(76, 9);
            this.label_lan2.Name = "label_lan2";
            this.label_lan2.Size = new System.Drawing.Size(33, 15);
            this.label_lan2.TabIndex = 14;
            this.label_lan2.Text = "LAN";
            // 
            // panel_dial
            // 
            this.panel_dial.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_dial.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_dial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_dial.Controls.Add(this.textBox_dial);
            this.panel_dial.Controls.Add(this.buttontest_dial);
            this.panel_dial.Controls.Add(this.label_dial2);
            this.panel_dial.Location = new System.Drawing.Point(37, 66);
            this.panel_dial.Name = "panel_dial";
            this.panel_dial.Size = new System.Drawing.Size(185, 116);
            this.panel_dial.TabIndex = 7;
            // 
            // textBox_dial
            // 
            this.textBox_dial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_dial.Location = new System.Drawing.Point(14, 27);
            this.textBox_dial.Multiline = true;
            this.textBox_dial.Name = "textBox_dial";
            this.textBox_dial.Size = new System.Drawing.Size(157, 44);
            this.textBox_dial.TabIndex = 8;
            // 
            // buttontest_dial
            // 
            this.buttontest_dial.Location = new System.Drawing.Point(63, 74);
            this.buttontest_dial.Name = "buttontest_dial";
            this.buttontest_dial.Size = new System.Drawing.Size(59, 23);
            this.buttontest_dial.TabIndex = 4;
            this.buttontest_dial.Text = "Test";
            this.buttontest_dial.UseVisualStyleBackColor = true;
            // 
            // label_dial2
            // 
            this.label_dial2.AutoSize = true;
            this.label_dial2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_dial2.Location = new System.Drawing.Point(62, 9);
            this.label_dial2.Name = "label_dial2";
            this.label_dial2.Size = new System.Drawing.Size(60, 15);
            this.label_dial2.TabIndex = 12;
            this.label_dial2.Text = "DIAL UP";
            // 
            // panel_gprs
            // 
            this.panel_gprs.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_gprs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_gprs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_gprs.Controls.Add(this.textBox_gprs);
            this.panel_gprs.Controls.Add(this.buttontest_gprs);
            this.panel_gprs.Controls.Add(this.label_gprs2);
            this.panel_gprs.Location = new System.Drawing.Point(230, 66);
            this.panel_gprs.Name = "panel_gprs";
            this.panel_gprs.Size = new System.Drawing.Size(185, 116);
            this.panel_gprs.TabIndex = 8;
            // 
            // textBox_gprs
            // 
            this.textBox_gprs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_gprs.Location = new System.Drawing.Point(14, 27);
            this.textBox_gprs.Multiline = true;
            this.textBox_gprs.Name = "textBox_gprs";
            this.textBox_gprs.Size = new System.Drawing.Size(157, 44);
            this.textBox_gprs.TabIndex = 9;
            // 
            // buttontest_gprs
            // 
            this.buttontest_gprs.Location = new System.Drawing.Point(63, 74);
            this.buttontest_gprs.Name = "buttontest_gprs";
            this.buttontest_gprs.Size = new System.Drawing.Size(59, 23);
            this.buttontest_gprs.TabIndex = 18;
            this.buttontest_gprs.Text = "Test";
            this.buttontest_gprs.UseVisualStyleBackColor = true;
            // 
            // label_gprs2
            // 
            this.label_gprs2.AutoSize = true;
            this.label_gprs2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_gprs2.Location = new System.Drawing.Point(69, 9);
            this.label_gprs2.Name = "label_gprs2";
            this.label_gprs2.Size = new System.Drawing.Size(45, 15);
            this.label_gprs2.TabIndex = 13;
            this.label_gprs2.Text = "GPRS";
            // 
            // page_keypad
            // 
            this.page_keypad.BackColor = System.Drawing.SystemColors.Control;
            this.page_keypad.Controls.Add(this.pictureBox63);
            this.page_keypad.Controls.Add(this.pictureBox62);
            this.page_keypad.Controls.Add(this.pictureBox61);
            this.page_keypad.Controls.Add(this.pictureBox60);
            this.page_keypad.Controls.Add(this.pictureBox59);
            this.page_keypad.Controls.Add(this.pictureBox58);
            this.page_keypad.Controls.Add(this.pictureBox57);
            this.page_keypad.Controls.Add(this.pictureBox56);
            this.page_keypad.Controls.Add(this.pictureBox55);
            this.page_keypad.Controls.Add(this.pictureBox54);
            this.page_keypad.Controls.Add(this.pictureBox53);
            this.page_keypad.Controls.Add(this.pictureBox52);
            this.page_keypad.Controls.Add(this.pictureBox51);
            this.page_keypad.Controls.Add(this.pictureBox50);
            this.page_keypad.Controls.Add(this.pictureBox49);
            this.page_keypad.Controls.Add(this.pictureBox48);
            this.page_keypad.Controls.Add(this.pictureBox47);
            this.page_keypad.Controls.Add(this.pictureBox46);
            this.page_keypad.Controls.Add(this.pictureBox45);
            this.page_keypad.Controls.Add(this.pictureBox44);
            this.page_keypad.Controls.Add(this.pictureBox43);
            this.page_keypad.Controls.Add(this.pictureBox42);
            this.page_keypad.Controls.Add(this.pictureBox41);
            this.page_keypad.Controls.Add(this.pictureBox40);
            this.page_keypad.Controls.Add(this.pictureBox39);
            this.page_keypad.Controls.Add(this.pictureBox38);
            this.page_keypad.Controls.Add(this.pictureBox37);
            this.page_keypad.Controls.Add(this.pictureBox36);
            this.page_keypad.Controls.Add(this.pictureBox35);
            this.page_keypad.Controls.Add(this.pictureBox34);
            this.page_keypad.Controls.Add(this.pictureBox33);
            this.page_keypad.Controls.Add(this.pictureBox32);
            this.page_keypad.Controls.Add(this.buttontest_keypad);
            this.page_keypad.Controls.Add(this.pictureBox31);
            this.page_keypad.Controls.Add(this.pictureBox30);
            this.page_keypad.Controls.Add(this.pictureBox29);
            this.page_keypad.Controls.Add(this.pictureBox28);
            this.page_keypad.Controls.Add(this.pictureBox27);
            this.page_keypad.Controls.Add(this.pictureBox26);
            this.page_keypad.Controls.Add(this.pictureBox25);
            this.page_keypad.Controls.Add(this.pictureBox24);
            this.page_keypad.Controls.Add(this.pictureBox23);
            this.page_keypad.Controls.Add(this.pictureBox22);
            this.page_keypad.Controls.Add(this.pictureBox20);
            this.page_keypad.Controls.Add(this.pictureBox21);
            this.page_keypad.Controls.Add(this.pictureBox19);
            this.page_keypad.Location = new System.Drawing.Point(4, 22);
            this.page_keypad.Name = "page_keypad";
            this.page_keypad.Size = new System.Drawing.Size(452, 454);
            this.page_keypad.TabIndex = 6;
            this.page_keypad.Text = "KEYPAD";
            // 
            // pictureBox63
            // 
            this.pictureBox63.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox63.Image")));
            this.pictureBox63.Location = new System.Drawing.Point(324, 151);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(16, 16);
            this.pictureBox63.TabIndex = 111;
            this.pictureBox63.TabStop = false;
            this.pictureBox63.Visible = false;
            // 
            // pictureBox62
            // 
            this.pictureBox62.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox62.Image")));
            this.pictureBox62.Location = new System.Drawing.Point(324, 151);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(16, 13);
            this.pictureBox62.TabIndex = 110;
            this.pictureBox62.TabStop = false;
            this.pictureBox62.Visible = false;
            // 
            // pictureBox61
            // 
            this.pictureBox61.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox61.Image")));
            this.pictureBox61.Location = new System.Drawing.Point(324, 129);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(16, 16);
            this.pictureBox61.TabIndex = 109;
            this.pictureBox61.TabStop = false;
            this.pictureBox61.Visible = false;
            // 
            // pictureBox60
            // 
            this.pictureBox60.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox60.Image")));
            this.pictureBox60.Location = new System.Drawing.Point(324, 129);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(16, 13);
            this.pictureBox60.TabIndex = 108;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.Visible = false;
            // 
            // pictureBox59
            // 
            this.pictureBox59.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox59.Image")));
            this.pictureBox59.Location = new System.Drawing.Point(324, 107);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(16, 16);
            this.pictureBox59.TabIndex = 107;
            this.pictureBox59.TabStop = false;
            this.pictureBox59.Visible = false;
            // 
            // pictureBox58
            // 
            this.pictureBox58.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox58.Image")));
            this.pictureBox58.Location = new System.Drawing.Point(324, 107);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(16, 13);
            this.pictureBox58.TabIndex = 106;
            this.pictureBox58.TabStop = false;
            this.pictureBox58.Visible = false;
            // 
            // pictureBox57
            // 
            this.pictureBox57.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox57.Image")));
            this.pictureBox57.Location = new System.Drawing.Point(324, 85);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(16, 16);
            this.pictureBox57.TabIndex = 105;
            this.pictureBox57.TabStop = false;
            this.pictureBox57.Visible = false;
            // 
            // pictureBox56
            // 
            this.pictureBox56.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox56.Image")));
            this.pictureBox56.Location = new System.Drawing.Point(324, 85);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(16, 13);
            this.pictureBox56.TabIndex = 104;
            this.pictureBox56.TabStop = false;
            this.pictureBox56.Visible = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox55.Image")));
            this.pictureBox55.Location = new System.Drawing.Point(276, 334);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(16, 16);
            this.pictureBox55.TabIndex = 103;
            this.pictureBox55.TabStop = false;
            this.pictureBox55.Visible = false;
            // 
            // pictureBox54
            // 
            this.pictureBox54.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox54.Image")));
            this.pictureBox54.Location = new System.Drawing.Point(276, 334);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(16, 13);
            this.pictureBox54.TabIndex = 102;
            this.pictureBox54.TabStop = false;
            this.pictureBox54.Visible = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox53.Image")));
            this.pictureBox53.Location = new System.Drawing.Point(276, 295);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(16, 16);
            this.pictureBox53.TabIndex = 101;
            this.pictureBox53.TabStop = false;
            this.pictureBox53.Visible = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox52.Image")));
            this.pictureBox52.Location = new System.Drawing.Point(276, 295);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(16, 13);
            this.pictureBox52.TabIndex = 100;
            this.pictureBox52.TabStop = false;
            this.pictureBox52.Visible = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox51.Image")));
            this.pictureBox51.Location = new System.Drawing.Point(276, 258);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(16, 16);
            this.pictureBox51.TabIndex = 99;
            this.pictureBox51.TabStop = false;
            this.pictureBox51.Visible = false;
            // 
            // pictureBox50
            // 
            this.pictureBox50.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox50.Image")));
            this.pictureBox50.Location = new System.Drawing.Point(276, 258);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(16, 13);
            this.pictureBox50.TabIndex = 98;
            this.pictureBox50.TabStop = false;
            this.pictureBox50.Visible = false;
            // 
            // pictureBox49
            // 
            this.pictureBox49.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox49.Image")));
            this.pictureBox49.Location = new System.Drawing.Point(276, 226);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(16, 16);
            this.pictureBox49.TabIndex = 97;
            this.pictureBox49.TabStop = false;
            this.pictureBox49.Visible = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox48.Image")));
            this.pictureBox48.Location = new System.Drawing.Point(276, 226);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(16, 13);
            this.pictureBox48.TabIndex = 96;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.Visible = false;
            // 
            // pictureBox47
            // 
            this.pictureBox47.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox47.Image")));
            this.pictureBox47.Location = new System.Drawing.Point(230, 226);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(16, 16);
            this.pictureBox47.TabIndex = 95;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Visible = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox46.Image")));
            this.pictureBox46.Location = new System.Drawing.Point(230, 226);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(16, 13);
            this.pictureBox46.TabIndex = 94;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Visible = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox45.Image")));
            this.pictureBox45.Location = new System.Drawing.Point(187, 223);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(16, 16);
            this.pictureBox45.TabIndex = 93;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Visible = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox44.Image")));
            this.pictureBox44.Location = new System.Drawing.Point(187, 223);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(16, 13);
            this.pictureBox44.TabIndex = 92;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Visible = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox43.Image")));
            this.pictureBox43.Location = new System.Drawing.Point(230, 368);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(16, 16);
            this.pictureBox43.TabIndex = 91;
            this.pictureBox43.TabStop = false;
            this.pictureBox43.Visible = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox42.Image")));
            this.pictureBox42.Location = new System.Drawing.Point(230, 368);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(16, 13);
            this.pictureBox42.TabIndex = 90;
            this.pictureBox42.TabStop = false;
            this.pictureBox42.Visible = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox41.Image")));
            this.pictureBox41.Location = new System.Drawing.Point(187, 368);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(16, 16);
            this.pictureBox41.TabIndex = 89;
            this.pictureBox41.TabStop = false;
            this.pictureBox41.Visible = false;
            // 
            // pictureBox40
            // 
            this.pictureBox40.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox40.Image")));
            this.pictureBox40.Location = new System.Drawing.Point(187, 368);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(16, 13);
            this.pictureBox40.TabIndex = 88;
            this.pictureBox40.TabStop = false;
            this.pictureBox40.Visible = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox39.Image")));
            this.pictureBox39.Location = new System.Drawing.Point(141, 368);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(16, 16);
            this.pictureBox39.TabIndex = 87;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.Visible = false;
            // 
            // pictureBox38
            // 
            this.pictureBox38.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox38.Image")));
            this.pictureBox38.Location = new System.Drawing.Point(141, 368);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(16, 13);
            this.pictureBox38.TabIndex = 86;
            this.pictureBox38.TabStop = false;
            this.pictureBox38.Visible = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox37.Image")));
            this.pictureBox37.Location = new System.Drawing.Point(230, 331);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(16, 16);
            this.pictureBox37.TabIndex = 85;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Visible = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox36.Image")));
            this.pictureBox36.Location = new System.Drawing.Point(230, 331);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(16, 13);
            this.pictureBox36.TabIndex = 84;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Visible = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox35.Image")));
            this.pictureBox35.Location = new System.Drawing.Point(187, 331);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(16, 16);
            this.pictureBox35.TabIndex = 83;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Visible = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox34.Image")));
            this.pictureBox34.Location = new System.Drawing.Point(187, 334);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(16, 13);
            this.pictureBox34.TabIndex = 82;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Visible = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox33.Image")));
            this.pictureBox33.Location = new System.Drawing.Point(141, 331);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(16, 16);
            this.pictureBox33.TabIndex = 81;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Visible = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox32.Image")));
            this.pictureBox32.Location = new System.Drawing.Point(141, 331);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(16, 13);
            this.pictureBox32.TabIndex = 80;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Visible = false;
            // 
            // buttontest_keypad
            // 
            this.buttontest_keypad.Enabled = false;
            this.buttontest_keypad.Location = new System.Drawing.Point(161, 415);
            this.buttontest_keypad.Name = "buttontest_keypad";
            this.buttontest_keypad.Size = new System.Drawing.Size(145, 34);
            this.buttontest_keypad.TabIndex = 79;
            this.buttontest_keypad.Text = "Test";
            this.buttontest_keypad.UseVisualStyleBackColor = true;
            // 
            // pictureBox31
            // 
            this.pictureBox31.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox31.Image")));
            this.pictureBox31.Location = new System.Drawing.Point(230, 292);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(16, 16);
            this.pictureBox31.TabIndex = 45;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.Visible = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox30.Image")));
            this.pictureBox30.Location = new System.Drawing.Point(230, 292);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(16, 13);
            this.pictureBox30.TabIndex = 44;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.Visible = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox29.Image")));
            this.pictureBox29.Location = new System.Drawing.Point(187, 295);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(16, 16);
            this.pictureBox29.TabIndex = 43;
            this.pictureBox29.TabStop = false;
            this.pictureBox29.Visible = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox28.Image")));
            this.pictureBox28.Location = new System.Drawing.Point(187, 292);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(16, 13);
            this.pictureBox28.TabIndex = 42;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.Visible = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox27.Image")));
            this.pictureBox27.Location = new System.Drawing.Point(141, 295);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(16, 16);
            this.pictureBox27.TabIndex = 41;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Visible = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox26.Image")));
            this.pictureBox26.Location = new System.Drawing.Point(141, 295);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(16, 13);
            this.pictureBox26.TabIndex = 40;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Visible = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox25.Image")));
            this.pictureBox25.Location = new System.Drawing.Point(230, 255);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(16, 16);
            this.pictureBox25.TabIndex = 39;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Visible = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox24.Image")));
            this.pictureBox24.Location = new System.Drawing.Point(230, 255);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(16, 13);
            this.pictureBox24.TabIndex = 38;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Visible = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox23.Image")));
            this.pictureBox23.Location = new System.Drawing.Point(187, 255);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(16, 16);
            this.pictureBox23.TabIndex = 37;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Visible = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox22.Image")));
            this.pictureBox22.Location = new System.Drawing.Point(187, 258);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(16, 13);
            this.pictureBox22.TabIndex = 36;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Visible = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox20.Image")));
            this.pictureBox20.Location = new System.Drawing.Point(141, 255);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(16, 13);
            this.pictureBox20.TabIndex = 35;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Visible = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox21.Image")));
            this.pictureBox21.Location = new System.Drawing.Point(141, 255);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(16, 16);
            this.pictureBox21.TabIndex = 34;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Visible = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox19.Image")));
            this.pictureBox19.Location = new System.Drawing.Point(96, 18);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(278, 391);
            this.pictureBox19.TabIndex = 0;
            this.pictureBox19.TabStop = false;
            // 
            // tabPage8
            // 
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(452, 454);
            this.tabPage8.TabIndex = 7;
            this.tabPage8.Text = "LCD";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // page_icc
            // 
            this.page_icc.BackColor = System.Drawing.SystemColors.Control;
            this.page_icc.Controls.Add(this.label_icc2);
            this.page_icc.Controls.Add(this.panel_icc);
            this.page_icc.Location = new System.Drawing.Point(4, 22);
            this.page_icc.Name = "page_icc";
            this.page_icc.Size = new System.Drawing.Size(452, 454);
            this.page_icc.TabIndex = 8;
            this.page_icc.Text = "ICC";
            // 
            // label_icc2
            // 
            this.label_icc2.AutoSize = true;
            this.label_icc2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_icc2.Location = new System.Drawing.Point(175, 35);
            this.label_icc2.Name = "label_icc2";
            this.label_icc2.Size = new System.Drawing.Size(102, 20);
            this.label_icc2.TabIndex = 5;
            this.label_icc2.Text = "Módulo ICC";
            // 
            // panel_icc
            // 
            this.panel_icc.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_icc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_icc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_icc.Controls.Add(this.textBox_icc);
            this.panel_icc.Controls.Add(this.estado_icc);
            this.panel_icc.Controls.Add(this.buttontest_icc);
            this.panel_icc.Location = new System.Drawing.Point(58, 66);
            this.panel_icc.Name = "panel_icc";
            this.panel_icc.Size = new System.Drawing.Size(334, 141);
            this.panel_icc.TabIndex = 4;
            // 
            // textBox_icc
            // 
            this.textBox_icc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_icc.Location = new System.Drawing.Point(73, 40);
            this.textBox_icc.Multiline = true;
            this.textBox_icc.Name = "textBox_icc";
            this.textBox_icc.Size = new System.Drawing.Size(190, 57);
            this.textBox_icc.TabIndex = 3;
            this.textBox_icc.TextChanged += new System.EventHandler(this.textBox_icc_TextChanged);
            // 
            // estado_icc
            // 
            this.estado_icc.AutoSize = true;
            this.estado_icc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado_icc.Location = new System.Drawing.Point(142, 15);
            this.estado_icc.Name = "estado_icc";
            this.estado_icc.Size = new System.Drawing.Size(57, 16);
            this.estado_icc.TabIndex = 2;
            this.estado_icc.Text = "Estado";
            // 
            // buttontest_icc
            // 
            this.buttontest_icc.Location = new System.Drawing.Point(144, 103);
            this.buttontest_icc.Name = "buttontest_icc";
            this.buttontest_icc.Size = new System.Drawing.Size(59, 23);
            this.buttontest_icc.TabIndex = 0;
            this.buttontest_icc.Text = "Test";
            this.buttontest_icc.UseVisualStyleBackColor = true;
            this.buttontest_icc.Click += new System.EventHandler(this.buttontest_icc_Click);
            // 
            // page_msr
            // 
            this.page_msr.BackColor = System.Drawing.SystemColors.Control;
            this.page_msr.Controls.Add(this.label_msr2);
            this.page_msr.Controls.Add(this.panel_msr);
            this.page_msr.Location = new System.Drawing.Point(4, 22);
            this.page_msr.Name = "page_msr";
            this.page_msr.Size = new System.Drawing.Size(452, 454);
            this.page_msr.TabIndex = 9;
            this.page_msr.Text = "MSR";
            // 
            // label_msr2
            // 
            this.label_msr2.AutoSize = true;
            this.label_msr2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_msr2.Location = new System.Drawing.Point(175, 35);
            this.label_msr2.Name = "label_msr2";
            this.label_msr2.Size = new System.Drawing.Size(111, 20);
            this.label_msr2.TabIndex = 6;
            this.label_msr2.Text = "Módulo MSR";
            // 
            // panel_msr
            // 
            this.panel_msr.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_msr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_msr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_msr.Controls.Add(this.estado_msr);
            this.panel_msr.Controls.Add(this.textBox_msr);
            this.panel_msr.Controls.Add(this.buttontest_msr);
            this.panel_msr.Location = new System.Drawing.Point(58, 66);
            this.panel_msr.Name = "panel_msr";
            this.panel_msr.Size = new System.Drawing.Size(334, 141);
            this.panel_msr.TabIndex = 5;
            // 
            // estado_msr
            // 
            this.estado_msr.AutoSize = true;
            this.estado_msr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado_msr.Location = new System.Drawing.Point(142, 15);
            this.estado_msr.Name = "estado_msr";
            this.estado_msr.Size = new System.Drawing.Size(57, 16);
            this.estado_msr.TabIndex = 7;
            this.estado_msr.Text = "Estado";
            // 
            // textBox_msr
            // 
            this.textBox_msr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_msr.Location = new System.Drawing.Point(73, 40);
            this.textBox_msr.Multiline = true;
            this.textBox_msr.Name = "textBox_msr";
            this.textBox_msr.Size = new System.Drawing.Size(190, 57);
            this.textBox_msr.TabIndex = 7;
            // 
            // buttontest_msr
            // 
            this.buttontest_msr.Location = new System.Drawing.Point(144, 103);
            this.buttontest_msr.Name = "buttontest_msr";
            this.buttontest_msr.Size = new System.Drawing.Size(59, 23);
            this.buttontest_msr.TabIndex = 3;
            this.buttontest_msr.Text = "Test";
            this.buttontest_msr.UseVisualStyleBackColor = true;
            this.buttontest_msr.Click += new System.EventHandler(this.buttontest_msr_Click);
            // 
            // page_rf
            // 
            this.page_rf.BackColor = System.Drawing.SystemColors.Control;
            this.page_rf.Controls.Add(this.label_rf2);
            this.page_rf.Controls.Add(this.panel_rf);
            this.page_rf.Location = new System.Drawing.Point(4, 22);
            this.page_rf.Name = "page_rf";
            this.page_rf.Size = new System.Drawing.Size(452, 454);
            this.page_rf.TabIndex = 10;
            this.page_rf.Text = "RF";
            // 
            // label_rf2
            // 
            this.label_rf2.AutoSize = true;
            this.label_rf2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_rf2.Location = new System.Drawing.Point(175, 35);
            this.label_rf2.Name = "label_rf2";
            this.label_rf2.Size = new System.Drawing.Size(96, 20);
            this.label_rf2.TabIndex = 8;
            this.label_rf2.Text = "Módulo RF";
            // 
            // panel_rf
            // 
            this.panel_rf.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_rf.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_rf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_rf.Controls.Add(this.estado_rf);
            this.panel_rf.Controls.Add(this.textBox_rf);
            this.panel_rf.Controls.Add(this.buttontest_rf);
            this.panel_rf.Location = new System.Drawing.Point(58, 66);
            this.panel_rf.Name = "panel_rf";
            this.panel_rf.Size = new System.Drawing.Size(334, 141);
            this.panel_rf.TabIndex = 7;
            // 
            // estado_rf
            // 
            this.estado_rf.AutoSize = true;
            this.estado_rf.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado_rf.Location = new System.Drawing.Point(142, 15);
            this.estado_rf.Name = "estado_rf";
            this.estado_rf.Size = new System.Drawing.Size(57, 16);
            this.estado_rf.TabIndex = 7;
            this.estado_rf.Text = "Estado";
            // 
            // textBox_rf
            // 
            this.textBox_rf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_rf.Location = new System.Drawing.Point(73, 40);
            this.textBox_rf.Multiline = true;
            this.textBox_rf.Name = "textBox_rf";
            this.textBox_rf.Size = new System.Drawing.Size(190, 57);
            this.textBox_rf.TabIndex = 7;
            // 
            // buttontest_rf
            // 
            this.buttontest_rf.Location = new System.Drawing.Point(144, 103);
            this.buttontest_rf.Name = "buttontest_rf";
            this.buttontest_rf.Size = new System.Drawing.Size(59, 23);
            this.buttontest_rf.TabIndex = 3;
            this.buttontest_rf.Text = "Test";
            this.buttontest_rf.UseVisualStyleBackColor = true;
            this.buttontest_rf.Click += new System.EventHandler(this.buttontest_rf_Click);
            // 
            // page_printer
            // 
            this.page_printer.BackColor = System.Drawing.SystemColors.Control;
            this.page_printer.Controls.Add(this.label_printer2);
            this.page_printer.Controls.Add(this.panel_printer);
            this.page_printer.Location = new System.Drawing.Point(4, 22);
            this.page_printer.Name = "page_printer";
            this.page_printer.Size = new System.Drawing.Size(452, 454);
            this.page_printer.TabIndex = 11;
            this.page_printer.Text = "PRINTER";
            // 
            // label_printer2
            // 
            this.label_printer2.AutoSize = true;
            this.label_printer2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_printer2.Location = new System.Drawing.Point(153, 35);
            this.label_printer2.Name = "label_printer2";
            this.label_printer2.Size = new System.Drawing.Size(149, 20);
            this.label_printer2.TabIndex = 10;
            this.label_printer2.Text = "Módulo PRINTER";
            // 
            // panel_printer
            // 
            this.panel_printer.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_printer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_printer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_printer.Controls.Add(this.estado_printer);
            this.panel_printer.Controls.Add(this.textBox_printer);
            this.panel_printer.Controls.Add(this.buttontest_printer);
            this.panel_printer.Enabled = false;
            this.panel_printer.Location = new System.Drawing.Point(58, 66);
            this.panel_printer.Name = "panel_printer";
            this.panel_printer.Size = new System.Drawing.Size(334, 141);
            this.panel_printer.TabIndex = 9;
            // 
            // estado_printer
            // 
            this.estado_printer.AutoSize = true;
            this.estado_printer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estado_printer.Location = new System.Drawing.Point(142, 15);
            this.estado_printer.Name = "estado_printer";
            this.estado_printer.Size = new System.Drawing.Size(57, 16);
            this.estado_printer.TabIndex = 7;
            this.estado_printer.Text = "Estado";
            // 
            // textBox_printer
            // 
            this.textBox_printer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_printer.Location = new System.Drawing.Point(73, 40);
            this.textBox_printer.Multiline = true;
            this.textBox_printer.Name = "textBox_printer";
            this.textBox_printer.Size = new System.Drawing.Size(190, 57);
            this.textBox_printer.TabIndex = 7;
            // 
            // buttontest_printer
            // 
            this.buttontest_printer.Location = new System.Drawing.Point(144, 103);
            this.buttontest_printer.Name = "buttontest_printer";
            this.buttontest_printer.Size = new System.Drawing.Size(59, 23);
            this.buttontest_printer.TabIndex = 3;
            this.buttontest_printer.Text = "Test";
            this.buttontest_printer.UseVisualStyleBackColor = true;
            this.buttontest_printer.Click += new System.EventHandler(this.buttontest_printer_Click);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_archivo});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1013, 24);
            this.menu.TabIndex = 16;
            this.menu.Text = "menuStrip1";
            // 
            // menu_archivo
            // 
            this.menu_archivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_itemreporte,
            this.salirToolStripMenuItem});
            this.menu_archivo.Name = "menu_archivo";
            this.menu_archivo.Size = new System.Drawing.Size(60, 20);
            this.menu_archivo.Text = "Archivo";
            // 
            // menu_itemreporte
            // 
            this.menu_itemreporte.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.item_guardarcomo});
            this.menu_itemreporte.Name = "menu_itemreporte";
            this.menu_itemreporte.Size = new System.Drawing.Size(115, 22);
            this.menu_itemreporte.Text = "Reporte";
            // 
            // item_guardarcomo
            // 
            this.item_guardarcomo.Name = "item_guardarcomo";
            this.item_guardarcomo.Size = new System.Drawing.Size(159, 22);
            this.item_guardarcomo.Text = "Guardar como...";
            this.item_guardarcomo.Click += new System.EventHandler(this.item_guardarcomo_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // textBox_imei
            // 
            this.textBox_imei.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_imei.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_imei.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_imei.Location = new System.Drawing.Point(74, 103);
            this.textBox_imei.Name = "textBox_imei";
            this.textBox_imei.Size = new System.Drawing.Size(110, 14);
            this.textBox_imei.TabIndex = 17;
            // 
            // textBox_modelo
            // 
            this.textBox_modelo.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_modelo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_modelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_modelo.Location = new System.Drawing.Point(74, 63);
            this.textBox_modelo.Name = "textBox_modelo";
            this.textBox_modelo.Size = new System.Drawing.Size(110, 14);
            this.textBox_modelo.TabIndex = 12;
            // 
            // textBox_serial
            // 
            this.textBox_serial.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_serial.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_serial.Location = new System.Drawing.Point(74, 83);
            this.textBox_serial.Name = "textBox_serial";
            this.textBox_serial.Size = new System.Drawing.Size(110, 14);
            this.textBox_serial.TabIndex = 13;
            // 
            // fecha
            // 
            this.fecha.AutoSize = true;
            this.fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha.Location = new System.Drawing.Point(3, 3);
            this.fecha.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(46, 16);
            this.fecha.TabIndex = 19;
            this.fecha.Text = "Fecha";
            // 
            // modelo
            // 
            this.modelo.AutoSize = true;
            this.modelo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelo.Location = new System.Drawing.Point(3, 63);
            this.modelo.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.modelo.Name = "modelo";
            this.modelo.Size = new System.Drawing.Size(54, 16);
            this.modelo.TabIndex = 14;
            this.modelo.Text = "Modelo";
            // 
            // serial
            // 
            this.serial.AutoSize = true;
            this.serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serial.Location = new System.Drawing.Point(3, 83);
            this.serial.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.serial.Name = "serial";
            this.serial.Size = new System.Drawing.Size(43, 16);
            this.serial.TabIndex = 15;
            this.serial.Text = "Serial";
            // 
            // imei
            // 
            this.imei.AutoSize = true;
            this.imei.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imei.Location = new System.Drawing.Point(3, 103);
            this.imei.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.imei.Name = "imei";
            this.imei.Size = new System.Drawing.Size(34, 16);
            this.imei.TabIndex = 18;
            this.imei.Text = "IMEI";
            // 
            // table_info
            // 
            this.table_info.ColumnCount = 2;
            this.table_info.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.table_info.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.table_info.Controls.Add(this.textBox_hactual, 1, 2);
            this.table_info.Controls.Add(this.textBox_hinicio, 1, 1);
            this.table_info.Controls.Add(this.h_actual, 0, 2);
            this.table_info.Controls.Add(this.h_inicio, 0, 1);
            this.table_info.Controls.Add(this.fecha, 0, 0);
            this.table_info.Controls.Add(this.textBox_fecha, 1, 0);
            this.table_info.Controls.Add(this.label1, 0, 6);
            this.table_info.Controls.Add(this.imei, 0, 5);
            this.table_info.Controls.Add(this.serial, 0, 4);
            this.table_info.Controls.Add(this.modelo, 0, 3);
            this.table_info.Controls.Add(this.textBox_tecnico, 1, 6);
            this.table_info.Controls.Add(this.textBox_imei, 1, 5);
            this.table_info.Controls.Add(this.textBox_serial, 1, 4);
            this.table_info.Controls.Add(this.textBox_modelo, 1, 3);
            this.table_info.Location = new System.Drawing.Point(31, 99);
            this.table_info.Name = "table_info";
            this.table_info.RowCount = 7;
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_info.Size = new System.Drawing.Size(203, 140);
            this.table_info.TabIndex = 20;
            // 
            // textBox_hactual
            // 
            this.textBox_hactual.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_hactual.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_hactual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_hactual.Location = new System.Drawing.Point(74, 43);
            this.textBox_hactual.Name = "textBox_hactual";
            this.textBox_hactual.Size = new System.Drawing.Size(110, 14);
            this.textBox_hactual.TabIndex = 25;
            // 
            // textBox_hinicio
            // 
            this.textBox_hinicio.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_hinicio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_hinicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_hinicio.Location = new System.Drawing.Point(74, 23);
            this.textBox_hinicio.Name = "textBox_hinicio";
            this.textBox_hinicio.Size = new System.Drawing.Size(110, 14);
            this.textBox_hinicio.TabIndex = 25;
            // 
            // h_actual
            // 
            this.h_actual.AutoSize = true;
            this.h_actual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h_actual.Location = new System.Drawing.Point(3, 43);
            this.h_actual.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.h_actual.Name = "h_actual";
            this.h_actual.Size = new System.Drawing.Size(61, 16);
            this.h_actual.TabIndex = 25;
            this.h_actual.Text = "H. Actual";
            // 
            // h_inicio
            // 
            this.h_inicio.AutoSize = true;
            this.h_inicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.h_inicio.Location = new System.Drawing.Point(3, 23);
            this.h_inicio.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.h_inicio.Name = "h_inicio";
            this.h_inicio.Size = new System.Drawing.Size(55, 16);
            this.h_inicio.TabIndex = 25;
            this.h_inicio.Text = "H. Inicio";
            // 
            // textBox_fecha
            // 
            this.textBox_fecha.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_fecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_fecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_fecha.Location = new System.Drawing.Point(74, 3);
            this.textBox_fecha.Name = "textBox_fecha";
            this.textBox_fecha.Size = new System.Drawing.Size(110, 14);
            this.textBox_fecha.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 123);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 16);
            this.label1.TabIndex = 22;
            this.label1.Text = "Técnico";
            // 
            // textBox_tecnico
            // 
            this.textBox_tecnico.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textBox_tecnico.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox_tecnico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_tecnico.Location = new System.Drawing.Point(74, 123);
            this.textBox_tecnico.Name = "textBox_tecnico";
            this.textBox_tecnico.Size = new System.Drawing.Size(110, 14);
            this.textBox_tecnico.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(756, 152);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 183);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(756, 366);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 183);
            this.pictureBox2.TabIndex = 22;
            this.pictureBox2.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(832, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 20);
            this.label4.TabIndex = 23;
            this.label4.Text = "Gráficas";
            // 
            // timer_session
            // 
            this.timer_session.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // check_printer
            // 
            this.check_printer.Image = ((System.Drawing.Image)(resources.GetObject("check_printer.Image")));
            this.check_printer.Location = new System.Drawing.Point(255, 35);
            this.check_printer.Name = "check_printer";
            this.check_printer.Size = new System.Drawing.Size(16, 13);
            this.check_printer.TabIndex = 89;
            this.check_printer.TabStop = false;
            // 
            // failed_printer
            // 
            this.failed_printer.Image = ((System.Drawing.Image)(resources.GetObject("failed_printer.Image")));
            this.failed_printer.Location = new System.Drawing.Point(255, 35);
            this.failed_printer.Name = "failed_printer";
            this.failed_printer.Size = new System.Drawing.Size(16, 16);
            this.failed_printer.TabIndex = 90;
            this.failed_printer.TabStop = false;
            // 
            // Form_PostTesting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1013, 581);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.table_info);
            this.Controls.Add(this.tabControl_module);
            this.Controls.Add(this.panel_communication);
            this.Controls.Add(this.menu);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form_PostTesting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Postesting";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_PostTesting_FormClosing);
            this.Load += new System.EventHandler(this.Form_Postesting_Load);
            this.panel_communication.ResumeLayout(false);
            this.panel_communication.PerformLayout();
            this.tabControl_module.ResumeLayout(false);
            this.page_general.ResumeLayout(false);
            this.page_general.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.check_rf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_rf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_wifi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_wifi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_msr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_msr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_lan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_lan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_keypad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_keypad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_icc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_icc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_gprs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_dial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_dial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_gprs)).EndInit();
            this.page_comunicacion.ResumeLayout(false);
            this.page_comunicacion.PerformLayout();
            this.panel_tcp.ResumeLayout(false);
            this.panel_tcp.PerformLayout();
            this.panel_wifi.ResumeLayout(false);
            this.panel_wifi.PerformLayout();
            this.panel_usb.ResumeLayout(false);
            this.panel_usb.PerformLayout();
            this.panel_lan.ResumeLayout(false);
            this.panel_lan.PerformLayout();
            this.panel_dial.ResumeLayout(false);
            this.panel_dial.PerformLayout();
            this.panel_gprs.ResumeLayout(false);
            this.panel_gprs.PerformLayout();
            this.page_keypad.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            this.page_icc.ResumeLayout(false);
            this.page_icc.PerformLayout();
            this.panel_icc.ResumeLayout(false);
            this.panel_icc.PerformLayout();
            this.page_msr.ResumeLayout(false);
            this.page_msr.PerformLayout();
            this.panel_msr.ResumeLayout(false);
            this.panel_msr.PerformLayout();
            this.page_rf.ResumeLayout(false);
            this.page_rf.PerformLayout();
            this.panel_rf.ResumeLayout(false);
            this.panel_rf.PerformLayout();
            this.page_printer.ResumeLayout(false);
            this.page_printer.PerformLayout();
            this.panel_printer.ResumeLayout(false);
            this.panel_printer.PerformLayout();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.table_info.ResumeLayout(false);
            this.table_info.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.check_printer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.failed_printer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_oppenp;
        private System.Windows.Forms.ComboBox port_name;
        private System.Windows.Forms.Button button_closep;
        private System.Windows.Forms.Panel panel_communication;
        private System.Windows.Forms.Label label_baudrate;
        private System.Windows.Forms.Label label_portname;
        public System.Windows.Forms.ComboBox parity;
        public System.Windows.Forms.ComboBox data_bits;
        private System.Windows.Forms.Label label_databits;
        private System.Windows.Forms.Label label_parity;
        private System.Windows.Forms.ComboBox baud_rate;
        public System.IO.Ports.SerialPort pos_serial;
        private System.Windows.Forms.TabControl tabControl_module;
        private System.Windows.Forms.TabPage page_comunicacion;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ProgressBar progressBar1;
        public System.Windows.Forms.TabPage page_general;
        private System.Windows.Forms.Panel panel_gprs;
        private System.Windows.Forms.Panel panel_dial;
        private System.Windows.Forms.Panel panel_wifi;
        private System.Windows.Forms.Panel panel_usb;
        private System.Windows.Forms.Label label_wifi2;
        private System.Windows.Forms.Label label_usb2;
        private System.Windows.Forms.Label label_lan2;
        private System.Windows.Forms.Label label_gprs2;
        private System.Windows.Forms.Label label_dial2;
        private System.Windows.Forms.Label label_rf;
        private System.Windows.Forms.Label label_wifi;
        private System.Windows.Forms.Label label_lan;
        private System.Windows.Forms.Label label_gprs;
        private System.Windows.Forms.Label label_dial;
        private System.Windows.Forms.Label label_msr;
        private System.Windows.Forms.Label label_keypad;
        private System.Windows.Forms.Label label_icc;
        private System.Windows.Forms.Button button_testg;
        private System.Windows.Forms.Button buttontest_wifi;
        private System.Windows.Forms.Button buttonsend_usb;
        private System.Windows.Forms.Button buttontest_lan;
        private System.Windows.Forms.Button buttontest_gprs;
        private System.Windows.Forms.Button buttontest_dial;
        private System.Windows.Forms.TabPage page_keypad;
        public System.Windows.Forms.PictureBox pictureBox26;
        public System.Windows.Forms.PictureBox pictureBox25;
        public System.Windows.Forms.PictureBox pictureBox24;
        public System.Windows.Forms.PictureBox pictureBox23;
        public System.Windows.Forms.PictureBox pictureBox22;
        public System.Windows.Forms.PictureBox pictureBox20;
        public System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox19;
        public System.Windows.Forms.PictureBox pictureBox36;
        public System.Windows.Forms.PictureBox pictureBox35;
        public System.Windows.Forms.PictureBox pictureBox34;
        public System.Windows.Forms.PictureBox pictureBox33;
        public System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Button buttontest_keypad;
        public System.Windows.Forms.PictureBox pictureBox31;
        public System.Windows.Forms.PictureBox pictureBox30;
        public System.Windows.Forms.PictureBox pictureBox29;
        public System.Windows.Forms.PictureBox pictureBox28;
        public System.Windows.Forms.PictureBox pictureBox27;
        public System.Windows.Forms.PictureBox pictureBox63;
        public System.Windows.Forms.PictureBox pictureBox62;
        public System.Windows.Forms.PictureBox pictureBox61;
        public System.Windows.Forms.PictureBox pictureBox60;
        public System.Windows.Forms.PictureBox pictureBox59;
        public System.Windows.Forms.PictureBox pictureBox58;
        public System.Windows.Forms.PictureBox pictureBox57;
        public System.Windows.Forms.PictureBox pictureBox56;
        public System.Windows.Forms.PictureBox pictureBox55;
        public System.Windows.Forms.PictureBox pictureBox54;
        public System.Windows.Forms.PictureBox pictureBox53;
        public System.Windows.Forms.PictureBox pictureBox52;
        public System.Windows.Forms.PictureBox pictureBox51;
        public System.Windows.Forms.PictureBox pictureBox50;
        public System.Windows.Forms.PictureBox pictureBox49;
        public System.Windows.Forms.PictureBox pictureBox48;
        public System.Windows.Forms.PictureBox pictureBox47;
        public System.Windows.Forms.PictureBox pictureBox46;
        public System.Windows.Forms.PictureBox pictureBox45;
        public System.Windows.Forms.PictureBox pictureBox44;
        public System.Windows.Forms.PictureBox pictureBox43;
        public System.Windows.Forms.PictureBox pictureBox42;
        public System.Windows.Forms.PictureBox pictureBox41;
        public System.Windows.Forms.PictureBox pictureBox40;
        public System.Windows.Forms.PictureBox pictureBox39;
        public System.Windows.Forms.PictureBox pictureBox38;
        public System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.TextBox textBox_wifi;
        private System.Windows.Forms.TextBox textBox_usb;
        private System.Windows.Forms.TextBox textBox_lan;
        private System.Windows.Forms.TextBox textBox_gprs;
        private System.Windows.Forms.TextBox textBox_dial;
        private System.Windows.Forms.TabPage page_icc;
        private System.Windows.Forms.TabPage page_msr;
        private System.Windows.Forms.TabPage page_rf;
        private System.Windows.Forms.Label label_icc2;
        private System.Windows.Forms.Panel panel_icc;
        private System.Windows.Forms.TextBox textBox_icc;
        private System.Windows.Forms.Label estado_icc;
        private System.Windows.Forms.Button buttontest_icc;
        private System.Windows.Forms.Label label_msr2;
        private System.Windows.Forms.Panel panel_msr;
        private System.Windows.Forms.Label estado_msr;
        private System.Windows.Forms.Button buttontest_msr;
        private System.Windows.Forms.Label label_rf2;
        private System.Windows.Forms.Panel panel_rf;
        private System.Windows.Forms.Label estado_rf;
        private System.Windows.Forms.TextBox textBox_rf;
        private System.Windows.Forms.Button buttontest_rf;
        public System.Windows.Forms.TextBox textBox_msr;
        private System.Windows.Forms.TabPage page_printer;
        private System.Windows.Forms.Label label_printer2;
        private System.Windows.Forms.Panel panel_printer;
        private System.Windows.Forms.Label estado_printer;
        private System.Windows.Forms.TextBox textBox_printer;
        private System.Windows.Forms.Button buttontest_printer;
        private System.Windows.Forms.Label label_printer;
        public System.Windows.Forms.Panel panel_lan;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem menu_archivo;
        private System.Windows.Forms.ToolStripMenuItem menu_itemreporte;
        private System.Windows.Forms.ToolStripMenuItem item_guardarcomo;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog save_pdf;
        public System.Windows.Forms.TextBox textBox_imei;
        public System.Windows.Forms.TextBox textBox_modelo;
        public System.Windows.Forms.TextBox textBox_serial;
        private System.Windows.Forms.Label fecha;
        private System.Windows.Forms.Label modelo;
        private System.Windows.Forms.Label serial;
        private System.Windows.Forms.Label imei;
        private System.Windows.Forms.TableLayoutPanel table_info;
        public System.Windows.Forms.TextBox textBox_fecha;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBox_tecnico;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label general;
        private System.Windows.Forms.Panel panel11;
        public System.Windows.Forms.PictureBox check_rf;
        private System.Windows.Forms.LinkLabel detail_rf;
        public System.Windows.Forms.PictureBox failed_rf;
        public System.Windows.Forms.PictureBox failed_wifi;
        public System.Windows.Forms.PictureBox check_wifi;
        public System.Windows.Forms.PictureBox failed_msr;
        public System.Windows.Forms.PictureBox check_msr;
        public System.Windows.Forms.PictureBox failed_lan;
        public System.Windows.Forms.PictureBox check_lan;
        public System.Windows.Forms.PictureBox failed_keypad;
        public System.Windows.Forms.PictureBox check_keypad;
        public System.Windows.Forms.PictureBox failed_icc;
        public System.Windows.Forms.PictureBox check_icc;
        public System.Windows.Forms.PictureBox failed_gprs;
        public System.Windows.Forms.PictureBox failed_dial;
        public System.Windows.Forms.PictureBox check_dial;
        public System.Windows.Forms.PictureBox check_gprs;
        private System.Windows.Forms.LinkLabel detail_wifi;
        private System.Windows.Forms.LinkLabel detail_printer;
        private System.Windows.Forms.LinkLabel detail_msr;
        private System.Windows.Forms.LinkLabel detail_lan;
        private System.Windows.Forms.LinkLabel detail_keypad;
        private System.Windows.Forms.LinkLabel detail_icc;
        private System.Windows.Forms.LinkLabel detail_gprs;
        private System.Windows.Forms.LinkLabel detail_dial;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel_tcp;
        private System.Windows.Forms.TextBox textBox_send_uart;
        private System.Windows.Forms.Button buttonsend_uart;
        private System.Windows.Forms.Label label_tcp2;
        private System.Windows.Forms.Button buttonrecv_uart;
        private System.Windows.Forms.TextBox textBox_recv_uart;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button buttonrecv_usb;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer_session;
        public System.Windows.Forms.TextBox textBox_hactual;
        public System.Windows.Forms.TextBox textBox_hinicio;
        private System.Windows.Forms.Label h_actual;
        private System.Windows.Forms.Label h_inicio;
        public System.Windows.Forms.PictureBox failed_printer;
        public System.Windows.Forms.PictureBox check_printer;
    }
}