﻿//PROYECTO1 SITIO UNO -> 15/03/2016
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using iTextSharp;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;


using System.Windows.Forms;

namespace SitioUno
{
    public partial class Form2_principal : Form
    {
        #region variables
            object data;
            string dato_reciv= string.Empty;
           // string valor;
            public static int cuenta;
            public bool test;
            string[] busca_dato;
            string apuntador;
            modulos ejemplo = new modulos();
        #endregion 
        #region parametros interfaz

            /*-------------------------------------------------------------------------------
             *                      Diseño Interfaz                                         
             *          LABEL                                                               
             *  label1          --> Etiqueta Port Name.
             *  label2          --> Etiqueta Baud Rate.  
             *  label3          --> Etiqueta Parity.
             *  label4          --> Etiqueta DataBits.
             *  label5          --> Etiqueta Modelo.
             *  label6          --> Etiqueta Serial.
             *  label7          --> Etiqueta
             *  label8          --> Etiqueta
             *  
             *  
             *          LINK LABEL
             *  linklabel1      --> Etiqueta Detalles -> Dial Up.
             *  linklabel2      --> Etiqueta Detalles -> Gprs.
             *  linklabel3      --> Etiqueta Detalles -> Icc.
             *  linklabel4      --> Etiqueta Detalles -> Keypad.
             *  linklabel5      --> Etiqueta Detalles -> Lan.
             *  linklabel6      --> Etiqueta Detalles -> Msr.
             *  linklabel7      --> Etiqueta Detalles -> Tcp.
             *  linklabel8      --> Etiqueta Detalles -> Rf.
             *  linklabel9      --> Etiqueta Detalles -> Wifi.
             *  
             *  
             *          PANEL
             *  panel1          --> Seleccion de parametros de Conexion Serial.
             *  panel2          --> Panel Modulo Icc.
             *  panel3          --> Panel Msr.
             *  panel4          --> Panel Rf.
             *  panel5          --> Panel Dial Up.
             *  panel6          --> Panel Gprs.
             *  panel7          --> Panel Lan.
             *  panel8          --> Panel Tcp.
             *  panel9          --> Panel Usb.
             *  panel10         --> Panel Wifi.
             *  
             * 
             *          PICTURE BOX
             *  Opcion: "General"
             *  pictureBox1     --> (✔) Dial Up. 
             *  pictureBox2     --> (X) Dial Up.
             *  pictureBox3     --> (✔) Gprs. 
             *  pictureBox4     --> (X) Gprs.
             *  pictureBox5     --> (✔) Icc. 
             *  pictureBox6     --> (X) Icc.
             *  pictureBox7     --> (✔) Keypad. 
             *  pictureBox8     --> (X) Keypad.
             *  pictureBox9     --> (✔) Lan. 
             *  pictureBox10    --> (X) Lan.
             *  pictureBox11    --> (✔) Msr. 
             *  pictureBox12    --> (X) Msr.
             *  pictureBox13    --> (✔) Tcp. 
             *  pictureBox14    --> (X) Tcp.
             *  pictureBox15    --> (✔) Wifi. 
             *  pictureBox16    --> (X) Wifi.
             *  pictureBox17    --> (✔) Rf. 
             *  pictureBox18    --> (X) Rf.
             *  pictureBox19    --> Imagen S90.
             *  
             *  Opcion: "Keypad"
             *  pictureBox20    --> (✔)Key 1.
             *  pictureBox21    --> (X) Key 1.
             *  pictureBox22    --> (✔)Key 2.
             *  pictureBox23    --> (X) Key 2.
             *  pictureBox24    --> (✔)Key 3.
             *  pictureBox25    -->(X) Key 3.
             *  pictureBox26    --> (✔)Key 4.
             *  pictureBox27    -->(X) Key 4.
             *  pictureBox28    --> (✔)Key 5.
             *  pictureBox29    -->(X) Key 5.
             *  pictureBox30    --> (✔)Key 6.
             *  pictureBox31    -->(X) Key 6.
             *  pictureBox32    --> (✔)Key 7.
             *  pictureBox33    -->(X) Key 7.
             *  pictureBox34    --> (✔)Key 8.
             *  pictureBox35    -->(X) Key 8.
             *  pictureBox36    --> (✔)Key 9.
             *  pictureBox37    -->(X) Key 9.
             *  pictureBox38    --> (✔)Key Cancel.
             *  pictureBox39    -->(X) Key Cancel.
             *  pictureBox40    --> (✔)Key 0.
             *  pictureBox41    -->(X) Key 0.
             *  pictureBox42    --> (✔)Key Clear.
             *  pictureBox43    -->(X) Key Clear.
             *  pictureBox44    --> (✔)Key Up.
             *  pictureBox45    -->(X) Key Up.
             *  pictureBox46    --> (✔)Key Down.
             *  pictureBox47    -->(X) Key Down.
             *  pictureBox48    --> (✔)Key Func.
             *  pictureBox49    -->(X) Key Func.
             *  pictureBox50    --> (✔)Key Menu.
             *  pictureBox51    -->(X) Key Menu.
             *  pictureBox52    --> (✔)Key Alpha.
             *  pictureBox53    -->(X) Key Alpha.
             *  pictureBox54    --> (✔)Key Enter.
             *  pictureBox55    -->(X) Key Enter.
             *  pictureBox56    --> (✔)Key 
             *  pictureBox57    -->(X) Key 
             *  pictureBox58    --> (✔)Key 
             *  pictureBox59    -->(X) Key 
             *  
             * 
             *          BUTTON
             *  button_oppen    --> Abre puerto.
             *  button_closep   --> Cierra puerto.
             *  button1         --> Test Icc.
             *  button2         --> Test Msr.
             *  button3         --> Test Rf.
             *  button4         --> Test General.
             *  button5         --> Test Dial Up.
             *  button6         --> Test Gprs.
             *  button7         --> Test Lan.
             *  button8         --> Test Tcp.
             *  button9         --> Test Usb.
             *  button10        --> Test Wifi.
             *  
             * 
             *          TAB CONTROL              
             *  tabControl1     --> Menu Principal de Modulos.
             *  
             *  
             *  
             * 
             * 
             * 
             * 
             *  
             *----------------------------------------------------------------------------*/


        #endregion

            public Form2_principal()
            {
                InitializeComponent();
                
                CheckForIllegalCrossThreadCalls = false;
            }
        
            private void Form2_Load(object sender, EventArgs e)
            {
                
                mostrar();
                button_oppenp.Enabled = false;
                button_closep.Enabled = false;
       
                textBox3.Enabled = false;
                textBox4.Enabled = false;
                MaximizeBox = false;
                Valor_predeterminados();

                /**
                 * Picture (✔) y (X)
                 */
                pictureBox1.Visible = false;
                pictureBox2.Visible = false;
                pictureBox3.Visible = false;
                pictureBox4.Visible = false;
                pictureBox5.Visible = false;
                pictureBox6.Visible = false;
                pictureBox7.Visible = false;
                pictureBox8.Visible = false;
                pictureBox9.Visible = false;
                pictureBox10.Visible = false;
                pictureBox11.Visible = false;
                pictureBox12.Visible = false;
                pictureBox13.Visible = false;
                pictureBox14.Visible = false;
                pictureBox15.Visible = false;
                pictureBox16.Visible = false;
                pictureBox18.Visible = false;
                pictureBox17.Visible = false;
                pictureBox19.Visible = true;
                pictureBox64.Visible = false;
                pictureBox65.Visible = false;

               

                /**
                 * LinkLabel Panel General
                 */
                linkLabel1.Enabled = false;
                linkLabel2.Enabled = false;
                linkLabel3.Enabled = false;
                linkLabel4.Enabled = false;
                linkLabel5.Enabled = false;
                linkLabel6.Enabled = false;
                linkLabel7.Enabled = false;
                linkLabel8.Enabled = false;
                linkLabel9.Enabled = false;

                /**
                 * LinkLabel Panel Modulos
                 */
                panel2.Enabled = false;
                panel3.Enabled = false;
                panel4.Enabled = false;
                panel5.Enabled = false;
                panel6.Enabled = false;
                panel7.Enabled = false;
                panel10.Enabled = false;
 
            }

        #region  Diseno Form
        
        private void button1_Click(object sender, EventArgs e)
        {
            

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                
            }
            CreandoPuerto();
            abrir_puerto();
            desmarcar_opciones();
            button_oppenp.Enabled = false;
            button_closep.Enabled = true;
            textBox3.Enabled = true;
            textBox4.Enabled = true;

            Data prueba = new Data();

            prueba.new_pdf();
            // Creamos el documento con el tamaño de página tradicional
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                       new FileStream(@"prueba.pdf", FileMode.Create));

            // Le colocamos el título y el autor
            // **Nota: Esto no será visible en el documento
            doc.AddTitle("Mi primer PDF");
            doc.AddCreator("Roberto Torres");

            // Abrimos el archivo
            doc.Open();

            // Creamos el tipo de Font que vamos utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Escribimos el encabezamiento en el documento
            doc.Add(new Paragraph("Mi primer documento PDF"));
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblPrueba = new PdfPTable(3);
            tblPrueba.WidthPercentage = 100;

            // Configuramos el título de las columnas de la tabla
            PdfPCell clNombre = new PdfPCell(new Phrase("Nombre", _standardFont));
            clNombre.BorderWidth = 0;
            clNombre.BorderWidthBottom = 0.75f;

            PdfPCell clApellido = new PdfPCell(new Phrase("Apellido", _standardFont));
            clApellido.BorderWidth = 0;
            clApellido.BorderWidthBottom = 0.75f;

            PdfPCell clPais = new PdfPCell(new Phrase("País", _standardFont));
            clPais.BorderWidth = 0;
            clPais.BorderWidthBottom = 0.75f;

            // Añadimos las celdas a la tabla
            tblPrueba.AddCell(clNombre);
            tblPrueba.AddCell(clApellido);
            tblPrueba.AddCell(clPais);

            // Llenamos la tabla con información
            clNombre = new PdfPCell(new Phrase("Roberto", _standardFont));
            clNombre.BorderWidth = 0;

            clApellido = new PdfPCell(new Phrase("Torres", _standardFont));
            clApellido.BorderWidth = 0;

            clPais = new PdfPCell(new Phrase("Puerto Rico", _standardFont));
            clPais.BorderWidth = 0;

            // Añadimos las celdas a la tabla
            tblPrueba.AddCell(clNombre);
            tblPrueba.AddCell(clApellido);
            tblPrueba.AddCell(clPais);
        }

        private void button_closep_Click(object sender, EventArgs e)
        {
            button_closep.Enabled = false;
            serialPort1.Close();
            Valor_predeterminados();
            port_name.SelectedItem = null;
            port_name.Enabled = true;
            baud_rate.Enabled = true;
            parity.Enabled = true;
            data_bits.Enabled = true;
            button_oppenp.Enabled = false;
            modulos ejemplo = new modulos();
            cuenta = 0;
        }
        

       

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        
        private void port_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            button_oppenp.Enabled = true;
        }

        #endregion

        #region Puertos
        
        /**
         *  mostrar() = Obtiene todos los puertos disponibles para la comunicación serial. 
         */
        void mostrar()
        {
            string [] ports = SerialPort.GetPortNames();
            port_name.Items.AddRange(ports);
        }


        /**
         * abrir_puerto() = Se abre el puerto para empezar la comunicación serial.
         */
        void abrir_puerto()
        {
            try
            {
                serialPort1.Open();
                MessageBox.Show("Se abrio puerto");           
            }
            catch (Exception ex)
            {
                string var = serialPort1.PortName;
                MessageBox.Show( " Error" );
            }
        }

       

        /**
         * Valor_predeterminados() = Se le asigna los valores por defecto a los campos de selección de acuerdo
         *                           a la posición que tenga en su definición dentro del comboBox.
         */
        void Valor_predeterminados()
        {
            baud_rate.SelectedIndex = 6;
            parity.SelectedIndex = 0;
            data_bits.SelectedIndex = 3;
       
        }

        /**
         * desmarcar_opciones() = Se le 
         */
        void desmarcar_opciones()
        {
            port_name.Enabled = false;
            baud_rate.Enabled= false;
            parity.Enabled = false;
            data_bits.Enabled = false;
        }

        /**
         * clear() = Deshabilita los pictureBox al iniciar de nuevo un test
         *           ya sea un test General o de algun módulo específico.
         */
        void clear()
        {
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;
            pictureBox4.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox8.Visible = false;
            pictureBox9.Visible = false;
            pictureBox10.Visible = false;
            pictureBox11.Visible = false;
            pictureBox12.Visible = false;
            pictureBox13.Visible = false;
            pictureBox14.Visible = false;
            pictureBox15.Visible = false;
            pictureBox16.Visible = false;
            pictureBox18.Visible = false;
            pictureBox17.Visible = false;
        }

        /**
         * CreandoPuerto() = Se le asigna los valores al serialPort  seleccionado por el usuario.
         */
        void CreandoPuerto()
        {
                data = port_name.SelectedItem;
                serialPort1.PortName = data.ToString();


                data = baud_rate.SelectedItem;
                serialPort1.BaudRate = Convert.ToInt32(data);

                data = data_bits.SelectedItem;
                serialPort1.DataBits = Convert.ToInt32(data);

                data = parity.SelectedItem;

                switch (data.ToString())
                {
                    case "None":
                        serialPort1.Parity = Parity.None;
                        break;

                    case "Odd":
                        serialPort1.Parity = Parity.Odd;
                        break;

                    case "Even":
                        serialPort1.Parity = Parity.Even;
                        break;

                    case "Mark":
                        serialPort1.Parity = Parity.Mark;
                        break;

                    case "Space":
                        serialPort1.Parity = Parity.Space;
                        break;
                }   
        }
     

        /**
         * print(int recibe) = De acuerdo a la variable: "recibe" se habilita (✔)  o (X) del módulo donde fué llamado el metodo.
         */
        public void print(int recibe)
        {
            MessageBox.Show(recibe.ToString());
            switch (recibe)
            {
                case 5:
                    pictureBox5.Visible = true;
                    break;
                case 6:
                    pictureBox6.Visible = true;
                    break;
                case 11:
                    pictureBox11.Visible = true;
                    break;
                case 12:
                    pictureBox12.Visible = true;
                    break;
                case 17:
                    pictureBox18.Visible = true;
                    break;
                case 18:
                    pictureBox17.Visible = true;
                    break;
                case 64:
                    pictureBox64.Visible = true;
                break;
                case 65:
                    pictureBox65.Visible = true;
                break;
                case 100:
                    pictureBox7.Visible = true;
                    break;
                case 101:
                    pictureBox8.Visible = true;
                    break;
            }

        }

       
        /**
         * print_key() = Muestra el estado de cada tecla del dispositivo y se refleja en el tabPage: "KEYPAD".
         */
        public void print_key()
        {
            string key = string.Empty;

            int i;
            for (i = 0; i < ejemplo.array_key.Length; i++)
            {
                key = ejemplo.array_key[i];
                switch (key)
                {
                    case "1":
                        pictureBox21.Visible = true;
                        break;

                    case "2":
                        pictureBox23.Visible = true;
                        break;
                    case "3":
                        pictureBox25.Visible = true;
                        break;
                    case "4":
                        pictureBox27.Visible = true;
                        break;
                    case "5":
                        pictureBox29.Visible = true;
                        break;
                    case "6":
                        pictureBox31.Visible = true;
                        break;
                    case "7":
                        pictureBox33.Visible = true;
                        break;
                    case "8":
                        pictureBox35.Visible = true;
                        break;
                    case "9":
                        pictureBox37.Visible = true;
                        break;
                }
            }
            // Identificando las teclas operativas mediante (✔).
                if(pictureBox27.Visible == false)
                {
                    pictureBox26.Visible = true;
                }
               /* if(pictureBox23.Visible != true)
                {
                    pictureBox22.Visible = true;
                }
                if (pictureBox25.Visible != true)
                {
                    pictureBox24.Visible = true;
                }
                if (pictureBox27.Visible != true)
                {
                    pictureBox26.Visible = true;
                }
                if (pictureBox29.Visible != true)
                {
                    pictureBox28.Visible = true;
                }
                if (pictureBox31.Visible != true)
                {
                    pictureBox30.Visible = true;
                }*/
        }

        

#endregion

        #region EVENTOS
        private void Form2_principal_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort1.Close();
        }


        #region Datos_Recibidos
        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string error_r;
            cuenta = cuenta + 1;
            MessageBox.Show(cuenta.ToString());
            dato_reciv = serialPort1.ReadExisting();
            char[] eliminar = { 'H', '@', '/', 'I', 'M', 'R', 'K', '|', '5', 'T', '4', 'R' }; //{} caracteres a eliminar, establecidos para la mensajeria entre POS y Aplicacion
          
            string cadena = string.Empty;
            
                           /*busca_dato: almacena el primer caracter de la cadena recibida en dato_reciv 
                                                               // de esta manera poder comparar dicho caracter con los posibles casos*/

            /**
             * Verificación de Primer Paquete de Envío
             * Recibe: "*"  -  Envía: "*", Comprueba el módulo de Comunicación UART
             */
            if (dato_reciv[0].ToString() == "*" && cuenta == 1)
            {
                serialPort1.Write("*");
            }
            if(cuenta >1)
            {
                busca_dato = dato_reciv.Split('|');
                apuntador = busca_dato[0];
                busca_dato = busca_dato[1].Split('#');
            }
            
            
            /*------------------------------------------------------------------------------
             * Primer Caracter recibido de la cadena:
             * / --> Modelo del Equipo.
             * @ --> Serial del Equipo.
             * CX --> Para los módulos de comunicacion se procede a realizar 2 comparaciones:
             *          Comparacion #1: Permite identificar que es una informacion de algun modulo de comunicacion. 
             *          Comparacion #2: Permite identificar cual modulo de comunicacion envia la informacion.
             *          
             * I --> Información del Módulo ICC
             * KXX --> Informacion del Módulo Keypad. 
             *         En este módulo se utulizará el segundo caracter para identificar cual letra esta danada
             * M --> Información del Módulo MSR.
             * 
             * 
             *  
             *----------------------------------------------------------------------------*/

            
            
            /**
             * Verificación de Primer Paquete de Envío 
             * @ Modelo # Serial # Printer # DialUp # USBH # USBD # LAN # GPRS # WIFI # RF # ICC # MSR #
             */
            if (apuntador == "5T4RT" && cuenta == 2 )
            {
                textBox3.Text = busca_dato[0];
                textBox3.Enabled = false;
                textBox4.Text = busca_dato[1];
                textBox4.Enabled = false;
                button4.Enabled = true;
                for( int j = 2; j  < (busca_dato.Length-1); j++)
                {
                    switch(busca_dato[j])
                    {
                        case "S":       
                            switch(j)
                            {
                                case 2:     //Printer 
                                    panel12.Enabled = true;
                                break;

                                case 3:     //DialUp
                                panel5.Enabled = true;
                                break;

                                case 4:     //UsbH

                                break;

                                case 5:     //UsbD

                                break;

                                case 6:     //Lan
                                panel7.Enabled = true;
                                break;

                                case 7:     //Gprs
                                panel6.Enabled = true;
                                break;

                                case 8:     //Wifi
                                panel10.Enabled = true;
                                break;

                                case 9:     //RF
                                panel4.Enabled = true;
                                break;

                                case 10:    //Icc
                                panel2.Enabled = true;
                                break;

                                case 11:    //Msr
                                panel3.Enabled = true;
                                break;
                            }
                        break;

                        case "N":
                            switch(j)
                            {
                                case 2:     //Printer 
                                    textBox12.Text = "No disponible para este modelo";
                                 break;

                                case 3:     //DialUp
                                    textBox6.Text = "No disponible para este modelo";
                                break;

                                case 4:     //UsbH

                                break;

                                case 5:     //UsbD

                                break;

                                case 6:     //Lan
                                    textBox8.Text = "No disponible para este modelo";
                                break;

                                case 7:     //Gprs
                                    textBox7.Text = "No disponible para este modelo";
                                break;

                                case 8:     //Wifi
                                    textBox11.Text = "No disponible para este modelo";
                                    break;

                                case 9:     //RF
                                    panel4.Enabled = true;
                                break;

                                case 10:    //Icc
                                    textBox1.Text = "No disponible para este modelo";
                                break;

                                case 11:    //Msr
                                    textBox2.Text = "No disponible para este modelo";
                                break;
                            }
                        break;
                        
                    }
                }
               
            }
            if (cuenta >= 3)
            {
               for (int i = 0; i < (busca_dato.Length - 1); i++)
                {
                    string campo = busca_dato[i]; //campo: almacena el primer caracter de la cadena recibida en dato_reciv 
                                                  //de esta manera poder comparar dicho caracter con los posibles casos
                    char dato = campo[0];
                    cadena = campo.TrimStart(eliminar);
                    switch (apuntador)
                    {
                        case "C":
                            //busca_dato = dato_reciv[1];
                            //casos para encontrar el modulo de comunicación a analizar
                            switch (busca_dato.ToString())
                            {
                                case "U":
                                    //comunication.UART();
                                break;
                            }
                            break;

                            case "H":               //modulo de Comunicaciones
                            //busca_dato = dato_reciv[1]
                            MessageBox.Show(busca_dato.ToString());
                            break;

                        case "8":               //modulo ICC
                            int m_icc = Convert.ToInt32(cadena);
                            m_icc = ejemplo.icc(m_icc);
                            if (test == true)
                            {
                                linkLabel3.Enabled = true;
                                print(m_icc);
                            }
                            
                        break;

                        case "9":               //módulo KEYPAD
                             string m_key =  cadena;
                             int m_key1 = ejemplo.keypad(m_key);
                             
                             
                             if( test == true)
                             {
                                 linkLabel4.Enabled = true;
                                 print(m_key1);
                             }
                             print_key();
                        break;

                        case "10":               //módulo MSR
                            int m_msr = Convert.ToInt32(cadena);
                            error_r = ejemplo.msr(m_msr);
                            if (test == true)
                            {
                                if (m_msr != 0)
                                {
                                    m_msr = 11;
                                }
                                else {m_msr = 12;}
                                linkLabel6.Enabled = true;
                                print(m_msr);
                            }
                            textBox2.Text = error_r;
                        break;

                        case "11":               //módulo PRINTER
                            int m_printer = Convert.ToInt32(cadena);
                            error_r = ejemplo.printer(m_printer);
                            if (test == true)
                            {
                                if (m_printer != 0)
                                {
                                    m_printer = 64;
                                }
                                else { m_printer = 65;  }
                                linkLabel10.Enabled = true;
                                print(m_printer);
                            }
                                textBox12.Text = error_r;
                        break;

                        case "12":              //módulo RF
                            int m_rf = Convert.ToInt32(cadena);
                            error_r = ejemplo.rf(m_rf);
                            if (test == true)
                            {
                                if (m_rf != 0)
                                {
                                    m_rf = 17;
                                }else {m_rf = 18;}
                                linkLabel9.Enabled = true;
                                print(m_rf);
                            }
                            textBox5.Text = error_r;
                        break;
                    }
                }
            }
        }
        #endregion

        

#endregion 

           
            private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
               //tabPage11.Show();
            }

            private void label5_Click(object sender, EventArgs e)
            {

            }

            private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 3;
             

            }

            private void button1_Click_1(object sender, EventArgs e)
            {
               // panel11.Enabled = false;
            }

            private void button2_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button3_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button5_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button6_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button7_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button8_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button9_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void button10_Click(object sender, EventArgs e)
            {
                panel11.Enabled = false;
            }

            private void linkLabel9_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 6;
            }

            private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 4;
            }

            private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 5;
            }

            private void linkLabel4_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 2;
            }

            private void button4_Click(object sender, EventArgs e)
            {
                test = true;

            }

            private void button11_Click(object sender, EventArgs e)
            {
                test = false;
            }

            private void pictureBox36_Click(object sender, EventArgs e)
            {

            }

            private void button11_Click_1(object sender, EventArgs e)
            {
                clear();
            }

            private void linkLabel10_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
            {
                tabControl1.SelectedIndex = 7;
            }

            private void pictureBox17_Click(object sender, EventArgs e)
            {

            }

            private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
            {
                saveFileDialog1.Filter = "txt files (*.txt)|*.txt";

                if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK
                    && saveFileDialog1.FileName.Length > 0)
                {

                    richTextBox1.SaveFile(saveFileDialog1.FileName,
                        RichTextBoxStreamType.PlainText);
                }
            }

    }

    class mayor
    {
        class hijo1
        {

        }
        class hijo2
        {

        }
    }
}


