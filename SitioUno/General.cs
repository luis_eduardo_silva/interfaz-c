﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;

using System.Windows.Forms;

namespace SitioUno
{
   public class processdata
   {

#region Variables
       public object data;
       string dato_reciv = string.Empty;
       public static int count;
       public bool test;
       string[] search_data;
       string apuntador;
#endregion

#region Instancias

       print printer = new print();         //Clase printer - General.cs
       value_module ejemplo = new value_module();       //Clase value_module - General.cs

#endregion


/*------------------------------------------------------------------------------
 *  Description   : Recibe y procesa toda la informacion enviada desde el Pos durante
 *                  la comunicacion serial. 
 *                  
 * Parametros     : Recibe los siguientes parametros:
 *                  Control del Form_PostTesting:
 *                      -Control SerialPort.
 *                      -Control Object.
 *                      -Control SerialDataReceivedEventArgs
 *                      -Control ComboBox.
 *                      -Control TextBox.
 *                      -Control ButtonBox.
 *                      -Control LinkLabel.
 *                      -Control Panel.
 *                      -Control PictureBox.
 *                  Variable:
 *                  - bool test -> Indica si el usuario va a realizar un test general
 *                                 o algun test de un modulo en espescifico. Esta variable
 *                                 se modifica de acuerdo a los button usados del tabControl.
 *                                 
 *                          
 *----------------------------------------------------------------------------*/
       public void serialPort_Post_DataReceived(object sender, SerialDataReceivedEventArgs e, SerialPort port, TextBox textBoxmodelo, 
                                            TextBox textBoxserial,Button buttontestg, Panel panelprinter,Panel paneldial, 
                                            Panel panellan, Panel panelgprs, Panel panelwifi, Panel panelrf, Panel panelicc,
                                            Panel panelmsr, TextBox textBoxprinter, TextBox textBoxdial, TextBox textBoxlan,
                                            TextBox textBoxgprs,TextBox textBoxwifi, TextBox textBoxrf, TextBox textBoxicc, 
                                            TextBox textBoxmsr,LinkLabel detailicc, LinkLabel detailkeypad, LinkLabel detailmsr,
                                             LinkLabel detailprinter,LinkLabel detailrf,
                                            PictureBox checkicc, PictureBox failedicc,
                                            PictureBox checkmsr, PictureBox failedmsr, PictureBox failedrf,
                                            PictureBox checkrf, PictureBox failedprinter, PictureBox checkprinter,
                                            PictureBox checkkeypad, PictureBox failedkeypad, bool test)
       {
           string error_r;
           count = count + 1;         //mediante el valor de count, se sabe el paquete de comunicacion recibido por el pos.      
       
           dato_reciv = port.ReadExisting();  //dato_reciv = almacena lo recibido de la comunicacion serial.
     
           char[] delete = { 'H', '@', '/', 'I', 'M', 'R', 'K', '|', '5', 'T', '4', 'R' }; //{} caracteres a eliminar, establecidos para la mensajeria entre POS y Aplicacion.

           string cadena = string.Empty;

           if (dato_reciv[0].ToString() == "*" && count == 1)   //primer paquete de comunicacion.
           {
               port.Write("*");
           }
           if (count > 1)           //elimina los caracteres (|) y (#) despues del primer paquete de comunicacion.
           {
               search_data = dato_reciv.Split('|');     //search_data = almacena la cadena de caracteres recibida del pos
                                                        //              eliminando (|).
               apuntador = search_data[0];              //apuntador =  almacena los caracteres antes del primer (|)
                                                        //              enviado y de esta manera saber que parte de la mensajeria
                                                        //              se va a procesar.

               search_data = search_data[1].Split('#');  //divide la cadena de caracteres donde haya (#).
           }
           if (apuntador == "5T4RT" && count == 2)      //segundo paquete de comunicacion.
           {
               textBoxmodelo.Enabled = true;
               textBoxmodelo.Text = search_data[0];
               textBoxmodelo.Enabled = false;
               textBoxserial.Enabled = true;
               textBoxserial.Text = search_data[1];
               textBoxserial.Enabled = false;
               buttontestg.Enabled = true;
               for (int j = 2; j < (search_data.Length - 1); j++)
               {
                   switch (search_data[j])
                   {
                       case "S":
                           switch (j)
                           {
                               case 2:     //Printer 
                                   panelprinter.Enabled = true;
                                   break;

                               case 3:     //DialUp
                                   paneldial.Enabled = true;
                                   break;

                               case 4:     //UsbH

                                   break;

                               case 5:     //UsbD

                                   break;

                               case 6:     //Lan
                                   panellan.Enabled = true;
                                   break;

                               case 7:     //Gprs
                                   panelgprs.Enabled = true;
                                   break;

                               case 8:     //Wifi
                                   panelwifi.Enabled = true;
                                   break;

                               case 9:     //RF
                                   panelrf.Enabled = true;
                                   break;

                               case 10:    //Icc
                                   panelicc.Enabled = true;
                                   break;

                               case 11:    //Msr
                                   panelmsr.Enabled = true;
                                   break;
                           }
                           break;

                       case "N":
                           switch (j)
                           {
                               case 2:     //Printer 
                                   textBoxprinter.Text = "No disponible para este modelo";
                                   break;

                               case 3:     //DialUp
                                   textBoxdial.Text = "No disponible para este modelo";
                                   break;

                               case 4:     //UsbH

                                   break;

                               case 5:     //UsbD

                                   break;

                               case 6:     //Lan
                                   textBoxlan.Text = "No disponible para este modelo";
                                   break;

                               case 7:     //Gprs
                                   textBoxgprs.Text = "No disponible para este modelo";
                                   
                               
                                   break;

                               case 8:     //Wifi
                                   textBoxwifi.Text = "No disponible para este modelo";
                                   break;

                               case 9:     //RF
                                   textBoxrf.Text = "No disponible para este modelo";
                                   break;

                               case 10:    //Icc
                                   textBoxicc.Text = "No disponible para este modelo";
                                   break;

                               case 11:    //Msr
                                   textBoxmsr.Text = "No disponible para este modelo";
                                   break;
                           }
                           break;
                   }
               }

           }
           if (count >= 3)      
            {
               for (int i = 0; i < (search_data.Length - 1); i++)
                {
                    string campo = search_data[i]; //campo: almacena  codigos de los modulos con el separador (#).

                    cadena = campo.TrimStart(delete); //cadena= almacena los codigos de los modulos sin el separador (#).

                    switch (apuntador)
                    {
                        case "8":               //modulo ICC
                            int m_icc = Convert.ToInt32(cadena);
                            m_icc = ejemplo.icc(m_icc);
                            if (test == true)
                            {
                                if(m_icc != 0 )
                                {
                                    m_icc = 6;
                                }
                                else { m_icc = 5; }
                                detailicc.Enabled = true;
                                printer.prin_ter(m_icc, checkicc, failedicc, checkmsr, failedmsr, failedrf, checkrf, failedprinter, checkprinter, checkkeypad, failedkeypad);
                            }
                            
                        break;

                        case "9":               //módulo KEYPAD
                             string m_key =  cadena;
                             int m_key1 = ejemplo.keypad(m_key);
                             
                             
                             if( test == true)
                             {
                                 detailkeypad.Enabled = true;
                                                                 ;
                             }
                             //print_key();
                        break;

                        case "10":               //módulo MSR
                            int m_msr = Convert.ToInt32(cadena);
                            error_r = ejemplo.msr(m_msr);
                            if (test == true)
                            {
                                if (m_msr != 0)
                                {
                                    m_msr = 11;
                                }
                                else {m_msr = 12;}
                                detailmsr.Enabled = true;

                                printer.prin_ter(m_msr,checkicc,failedicc,checkmsr, failedmsr, failedrf, checkrf, failedprinter, checkprinter, checkkeypad, failedkeypad);
                            }
                                textBoxmsr.Text = error_r;
                        break;

                        case "11":               //módulo PRINTER
                            int m_printer = Convert.ToInt32(cadena);
                            error_r = ejemplo.printer(m_printer);
                            if (test == true)
                            {
                                if (m_printer != 0)
                                {
                                    m_printer = 64;
                                }
                                else { m_printer = 65;  }
                                detailprinter.Enabled = true;
                                printer.prin_ter(m_printer, checkicc, failedicc, checkmsr, failedmsr, failedrf, checkrf, failedprinter, checkprinter, checkkeypad, failedkeypad);
                            }
                                textBoxprinter.Text = error_r;
                        break;

                        case "12":              //módulo RF
                            int m_rf = Convert.ToInt32(cadena);
                            error_r = ejemplo.rf(m_rf);
                            if (test == true)
                            {
                                if (m_rf != 0)
                                {
                                    m_rf = 17;
                                }else {m_rf = 18;}
                                detailrf.Enabled = true;
                                printer.prin_ter(m_rf, checkicc, failedicc, checkmsr, failedmsr, failedrf, checkrf, failedprinter, checkprinter, checkkeypad, failedkeypad);
                            }
                            textBoxrf.Text = error_r;
                        break;
                    }
                }
            }
       }
   }

    public class serial_port
    {
/*------------------------------------------------------------------------------
 *  Description   : Muestra en el panel de opciones de comunicacion serial, los
 *                  puertos disponibles. 
 *                  
 * Parametros     : Recibe los siguientes parametros de control del Form_PostTesting:
 *                  -Control ComboBox.
 *----------------------------------------------------------------------------*/
        public void view_com(ComboBox combo )
        {
            string[] ports = SerialPort.GetPortNames();
            combo.Items.AddRange(ports);
            
        }

/*------------------------------------------------------------------------------
 *  Description   : Abre el puerto para iniciar la comunicacion serial entre el Pos
 *                  y la aplicacion.
 *                  
 * Parametros     : Recibe los siguientes parametros de control del Form_PostTesting:
 *                  -Control SerialPort.
 *----------------------------------------------------------------------------*/
        public void open_port(SerialPort serial)
        {
            try
            {
                serial.Open();
                MessageBox.Show("Se abrio puerto");
            }
            catch (Exception ex)
            {
             
                MessageBox.Show(" Error");
            }
        }

/*------------------------------------------------------------------------------
 *  Description   : Se definen los parametros del puerto que se va a usar
 *                  para la comunicacion. Estos parametros son los escogidos
 *                  por el usuario.
 *                  
 * Parametros     : Recibe los siguientes parametros de control del Form_PostTesting:
 *                  -Control SerialPort.
 *                  -Control ComboBox.
 *----------------------------------------------------------------------------*/
        public void create_port(SerialPort serial,ComboBox portname, ComboBox baudrate, ComboBox databits, ComboBox parity )
        {
            object data;

            data = portname.SelectedItem;           
            serial.PortName = data.ToString();      //Configuracion PortName - Valor Seleccionado del Panel.


            data = baudrate.SelectedItem;
            serial.BaudRate = Convert.ToInt32(data);        //Configuracion BaudRate - Valor Seleccionado del Panel.

            data = databits.SelectedItem;
            serial.DataBits = Convert.ToInt32(data);

            data = parity.SelectedItem;             //Configuracion Parity - Valor Seleccionado del Panel.

            switch (data.ToString())            
            {
                case "None":
                    serial.Parity = Parity.None;
                    break;

                case "Odd":
                    serial.Parity = Parity.Odd;
                    break;

                case "Even":
                    serial.Parity = Parity.Even;
                    break;

                case "Mark":
                    serial.Parity = Parity.Mark;
                    break;

                case "Space":
                    serial.Parity = Parity.Space;
                    break;
            }
        }
    }

    public class valuedefault
    {

/*------------------------------------------------------------------------------
 *  Description   : Coloca valores predeterminados en el panel de opciones 
 *                  de comunicacion serial. Estos valores se colocan de acuerdo 
 *                  la posicion que tenga en la definicion del comboBox.
 *                  
 * Parametros     : Recibe los siguientes parametros de control del Form_PostTesting:
 *                  -Control ComboBox.
 *----------------------------------------------------------------------------*/
        public void dafault_value(ComboBox baudrate, ComboBox parity, ComboBox databits)
        {
            baudrate.SelectedIndex = 8;
            parity.SelectedIndex = 0;
            databits.SelectedIndex = 3;
        }

/*------------------------------------------------------------------------------
 *  Description   : Deshabilita las opciones de cada valor del panel de opciones de
 *                  comunicacion serial.
 *                  
 * Parametros     : Recibe los siguientes parametros de control del Form_PostTesting:
 *                  -Control ComboBox.
 *----------------------------------------------------------------------------*/
        public void disabled_option(ComboBox portname, ComboBox baudrate, ComboBox parity, ComboBox databits)
        {
            portname.Enabled = false;
            baudrate.Enabled = false;
            parity.Enabled = false;
            databits.Enabled = false;
        }
    }

    public class print
    {
   /*------------------------------------------------------------------------------
    *  Description   : Muestra el estado actual del modulo en el panel de test general
    *                  haciendo visible las imagenes: (✔) o (X).
    *  
    *                  
    *  Parametros    : Recibe los siguientes parametros:
    *                  - Parametros de Control de Form_PostTesting 
    *                    PictureBox.
    *                  - int recibe -> Indica que imagen se debe mostrar.
    *----------------------------------------------------------------------------*/
        public void prin_ter(int recibe, PictureBox checkicc, PictureBox failedicc,
                                PictureBox checkmsr, PictureBox failedmsr, PictureBox failedrf,
                                PictureBox checkrf, PictureBox failedprinter, PictureBox checkprinter,
                                PictureBox checkkeypad,PictureBox failedkeypad)
        {
            switch (recibe)
            {
                case 5:
                    checkicc.Visible = true;
                    break;
                case 6:
                    failedicc.Visible = true;
                    break;
                case 11:
                    checkmsr.Visible = true;
                    break;
                case 12:
                    failedmsr.Visible = true;
                    break;
                case 17:
                    failedrf.Visible = true;
                    break;
                case 18:
                    checkrf.Visible = true;
                    break;
                case 64:
                    failedprinter.Visible = true;
                    break;
                case 65:
                    checkprinter.Visible = true;
                    break;
                case 100:
                    checkkeypad.Visible = true;
                    break;
                case 101:
                    failedkeypad.Visible = true;
                    break;
            }
        }
    }

    public class clear
    {
        public void clean_image(PictureBox check_dial, PictureBox failed_dial, PictureBox check_gprs,
                                PictureBox failed_gprs, PictureBox check_icc, PictureBox failed_icc,
                                PictureBox check_keypad, PictureBox failed_keypad, PictureBox check_lan,
                                PictureBox failed_lan, PictureBox check_msr, PictureBox failed_msr,
                                PictureBox check_tcp, PictureBox failed_tcp, PictureBox check_wifi,
                                PictureBox failed_wifi, PictureBox failed_rf, PictureBox check_rf)
        {
            check_dial.Visible = false;
            failed_dial.Visible = false;
            check_gprs.Visible = false;
            failed_gprs.Visible = false;
            check_icc.Visible = false;
            failed_icc.Visible = false;
            check_keypad.Visible = false;
            failed_keypad.Visible = false;
            check_lan.Visible = false;
            failed_lan.Visible = false;
            check_msr.Visible = false;
            failed_msr.Visible = false;
            check_tcp.Visible = false;
            failed_tcp.Visible = false;
            check_wifi.Visible = false;
            failed_wifi.Visible = false;
            failed_rf.Visible = false;
            check_rf.Visible = false;
        }
    }

}
