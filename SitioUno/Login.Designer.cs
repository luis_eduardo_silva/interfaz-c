﻿namespace SitioUno
{
    partial class Form_login
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_login));
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.label_user = new System.Windows.Forms.Label();
            this.label_pass = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.button_login = new System.Windows.Forms.Button();
            this.panel_login = new System.Windows.Forms.Panel();
            this.linkLabe_forgot = new System.Windows.Forms.LinkLabel();
            this.picture_logo = new System.Windows.Forms.PictureBox();
            this.panel_login.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_logo)).BeginInit();
            this.SuspendLayout();
            // 
            // label_user
            // 
            this.label_user.AutoSize = true;
            this.label_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_user.Location = new System.Drawing.Point(70, 17);
            this.label_user.Name = "label_user";
            this.label_user.Size = new System.Drawing.Size(62, 16);
            this.label_user.TabIndex = 1;
            this.label_user.Text = "Usuario";
            // 
            // label_pass
            // 
            this.label_pass.AutoSize = true;
            this.label_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_pass.Location = new System.Drawing.Point(59, 77);
            this.label_pass.Name = "label_pass";
            this.label_pass.Size = new System.Drawing.Size(87, 16);
            this.label_pass.TabIndex = 2;
            this.label_pass.Text = "Contraseña";
            this.label_pass.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(25, 42);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(156, 20);
            this.username.TabIndex = 3;
            this.username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.key);
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(25, 102);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(156, 20);
            this.password.TabIndex = 4;
            this.password.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.key);
            // 
            // button_login
            // 
            this.button_login.Location = new System.Drawing.Point(69, 141);
            this.button_login.Name = "button_login";
            this.button_login.Size = new System.Drawing.Size(65, 22);
            this.button_login.TabIndex = 6;
            this.button_login.Text = "Entrar";
            this.button_login.UseVisualStyleBackColor = true;
            this.button_login.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel_login
            // 
            this.panel_login.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel_login.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_login.Controls.Add(this.linkLabe_forgot);
            this.panel_login.Controls.Add(this.label_user);
            this.panel_login.Controls.Add(this.button_login);
            this.panel_login.Controls.Add(this.label_pass);
            this.panel_login.Controls.Add(this.password);
            this.panel_login.Controls.Add(this.username);
            this.panel_login.Location = new System.Drawing.Point(42, 131);
            this.panel_login.Name = "panel_login";
            this.panel_login.Size = new System.Drawing.Size(203, 205);
            this.panel_login.TabIndex = 7;
            // 
            // linkLabe_forgot
            // 
            this.linkLabe_forgot.AutoSize = true;
            this.linkLabe_forgot.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabe_forgot.Location = new System.Drawing.Point(26, 175);
            this.linkLabe_forgot.Name = "linkLabe_forgot";
            this.linkLabe_forgot.Size = new System.Drawing.Size(148, 16);
            this.linkLabe_forgot.TabIndex = 8;
            this.linkLabe_forgot.TabStop = true;
            this.linkLabe_forgot.Text = "¿Olvidó su contraseña?";
            // 
            // picture_logo
            // 
            this.picture_logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picture_logo.Image = ((System.Drawing.Image)(resources.GetObject("picture_logo.Image")));
            this.picture_logo.Location = new System.Drawing.Point(68, 33);
            this.picture_logo.Name = "picture_logo";
            this.picture_logo.Size = new System.Drawing.Size(156, 77);
            this.picture_logo.TabIndex = 8;
            this.picture_logo.TabStop = false;
            // 
            // Form_login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 392);
            this.Controls.Add(this.picture_logo);
            this.Controls.Add(this.panel_login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form_login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Form_login_Load);
            this.panel_login.ResumeLayout(false);
            this.panel_login.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture_logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.Label label_user;
        private System.Windows.Forms.Label label_pass;
        private System.Windows.Forms.Button button_login;
        public System.Windows.Forms.TextBox username;
        public System.Windows.Forms.TextBox password;
        private System.Windows.Forms.LinkLabel linkLabe_forgot;
        private System.Windows.Forms.PictureBox picture_logo;
        public System.Windows.Forms.Panel panel_login;

    }
}

