﻿//PROYECTO1 SITIO UNO -> 04/04/2016
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.Timers;

using System.Windows.Forms;

namespace SitioUno
{
    public partial class Form_PostTesting : Form
    {

            public Form_PostTesting()
            {
                InitializeComponent();
                CheckForIllegalCrossThreadCalls = false;
            }

            private void Form_Postesting_Load(object sender, EventArgs e)
            {
                port.view_com(port_name);         //se muestran todos los puertos disponibles para la comunicacion serial.
                button_oppenp.Enabled = false;
                button_closep.Enabled = false;

                textBox_modelo.Enabled = false;
                textBox_serial.Enabled = false;
                textBox_imei.Enabled = false;
                textBox_fecha.Enabled = false;
                textBox_tecnico.Enabled = false;
                MaximizeBox = false;
                value.default_value(baud_rate, parity, data_bits);
                timer_session.Start();
                textBox_hinicio.Text = string.Format("{0:hh:mm:ss}", DateTime.Now);
 
                /**
                 * Picture (✔) y (X)
                 */
                check_dial.Visible = false;
                failed_dial.Visible = false;
                check_gprs.Visible = false;
                failed_gprs.Visible = false;
                check_icc.Visible = false;
                failed_icc.Visible = false;
                check_keypad.Visible = false;
                failed_keypad.Visible = false;
                check_lan.Visible = false;
                failed_lan.Visible = false;
                check_msr.Visible = false;
                failed_msr.Visible = false;
                
                check_wifi.Visible = false;
                failed_wifi.Visible = false;
                failed_rf.Visible = false;
                check_rf.Visible = false;
                pictureBox19.Visible = true;
                failed_printer.Visible = false;
                check_printer.Visible = false;

               

                /**
                 * LinkLabel Panel General
                 */
                detail_dial.Enabled = false;
                detail_gprs.Enabled = false;
                detail_icc.Enabled = false;
                detail_keypad.Enabled = false;
                detail_lan.Enabled = false;
                detail_msr.Enabled = false;
                detail_printer.Enabled = false;
                detail_wifi.Enabled = false;
                detail_rf.Enabled = false;

                /**
                 * LinkLabel Panel Modulos
                 */
                panel_icc.Enabled = false;
                panel_msr.Enabled = false;
                panel_rf.Enabled = false;
                panel_dial.Enabled = false;
                panel_gprs.Enabled = false;
                panel_lan.Enabled = false;
                panel_wifi.Enabled = false;
                
            }


#region variables
            public bool test;       /*Mediante esta variable comprobamos si el usuario va a realizar
                                      un test general o algun test de un modulo especifico*/
            public object data;
            string dato_reciv = string.Empty;
            public static int count;
            string[] search_data;
            string apuntador;
            int x = 0;
            int y = 0;
            

            #endregion

#region Instancias
            value_module ejemplo = new value_module();  //Clase value_module - General.cs
            serial_port port = new serial_port();     //Clase serial_port - General.cs
            valuedefault value = new valuedefault();    //Clase valuedefault  - General.cs
            print printer = new print();                //Clase printer - General.cs
            
            clear clear_image = new clear();
            Report report_pdf = new Report();
            #endregion      
    
#region EVENTOS
        
        //button_click
        private void button1_Click(object sender, EventArgs e)
        {
            port.create_port(pos_serial, port_name, baud_rate, data_bits, parity);    //Create_port: se crea el puerto con los parametros seleccionados.
            port.open_port(pos_serial);       //open_port: abre el puerto seleccionado.
            value.disabled_option(port_name, baud_rate, parity, data_bits);     //deshabilita las opciones del panel con los parametros de la com. serial.
            button_oppenp.Enabled = false;
            button_closep.Enabled = true;
        }

        private void button_closep_Click(object sender, EventArgs e)
        {
            button_closep.Enabled = false;
            pos_serial.Close();     //se cierra el puerto seleccionado para la comunicacion serial.
            port_name.Enabled = true;
            baud_rate.Enabled = true;
            parity.Enabled = true;
            data_bits.Enabled = true;
            button_oppenp.Enabled = true;
            value_module ejemplo = new value_module();
          
        }

        private void buttontest_msr_Click(object sender, EventArgs e)
        {

            test = false;

            clear_image.clean_image(check_dial, failed_dial, check_gprs,
                                    failed_gprs, check_icc, failed_icc,
                                    check_keypad, failed_keypad, check_lan,
                                    failed_lan, check_msr, failed_msr,
                                    check_wifi,
                                    failed_wifi, failed_rf, check_rf);
        }

        private void buttontest_printer_Click(object sender, EventArgs e)
        {
            test = false;

            clear_image.clean_image(check_dial, failed_dial, check_gprs,
                                    failed_gprs, check_icc, failed_icc,
                                    check_keypad, failed_keypad, check_lan,
                                    failed_lan, check_msr, failed_msr,
                                     check_wifi,
                                    failed_wifi, failed_rf, check_rf);
        }

        private void buttontest_rf_Click(object sender, EventArgs e)
        {
            test = false;

            clear_image.clean_image(check_dial, failed_dial, check_gprs,
                                    failed_gprs, check_icc, failed_icc,
                                    check_keypad, failed_keypad, check_lan,
                                    failed_lan, check_msr, failed_msr,
                                     check_wifi,
                                    failed_wifi, failed_rf, check_rf);
        }

        private void buttontest_icc_Click(object sender, EventArgs e)
        {
            test = false;

            clear_image.clean_image(check_dial, failed_dial, check_gprs,
                                    failed_gprs, check_icc, failed_icc,
                                    check_keypad, failed_keypad, check_lan,
                                    failed_lan, check_msr, failed_msr,
                                     check_wifi,
                                    failed_wifi, failed_rf, check_rf);
        }


        //Linklabe_click
        private void detailrf_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl_module.SelectedIndex = 6;
        }

        private void detailicc_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl_module.SelectedIndex = 4;
        }

        private void detailkeypad_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl_module.SelectedIndex = 2;
        }

        private void detailprinter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl_module.SelectedIndex = 7;
        }


        //Datos Recibidos - Comunicación Serial
        private void pos_serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {

           Thread.Sleep(1000);
           string error_r;  

           dato_reciv = pos_serial.ReadExisting();  //dato_reciv = almacena lo recibido de la comunicacion serial.

        //   char[] delete = { 'H', '@', '/', 'I', 'M', 'R', 'K', '|', '5', 'T', '4', 'R' }; //{} caracteres a eliminar, establecidos para la mensajeria entre POS y Aplicacion.

               string cadena = string.Empty;

           
                                                              //elimina los caracteres (|) y (#) despues del primer paquete de comunicacion.

               search_data = dato_reciv.Split('|');           //search_data = almacena la cadena de caracteres recibida del pos
                                                              //              eliminando (|).
               apuntador = search_data[0];                    //apuntador =  almacena los caracteres antes del primer (|)
                                                              //              enviado y de esta manera saber que parte de la mensajeria
                                                              //              se va a procesar.
               search_data = search_data[1].Split('#');       //divide la cadena de caracteres donde haya (#).



            if (apuntador == "*")   //primer paquete de comunicacion.
            {
                pos_serial.Write("*");
            }

           if (apuntador == "5T4RT")      //segundo paquete de comunicacion.
           {
               textBox_modelo.Enabled = true;
               textBox_modelo.Text = search_data[0];
               textBox_modelo.Enabled = false;
               textBox_serial.Enabled = true;
               textBox_serial.Text = search_data[1];
               textBox_serial.Enabled = false;
               button_testg.Enabled = true;
               int pos = 0;

               valuedefault row = new valuedefault();
               valuedefault column = new valuedefault();

               for (int j = 2; j < (search_data.Length - 1); j++)
               {
                   switch (search_data[j])
                   {
                       case "S":
                           switch (j)
                           {
                               case 2:     //Printer 
                                   panel_printer.Enabled = true;
                                   break;

                               case 3:     //DialUp
                                   panel_dial.Enabled = true;
                                   pos++;
                                   x = column.sel_column(pos);
                                   y = column.sel_column(pos);
                                   panel_dial.Location = new Point(x,y);

                                   break;

                               case 4:     //UsbH

                                   break;

                               case 5:     //UsbD

                                   break;

                               case 6:     //Lan
                                   panel_lan.Enabled = true;
                                   pos++;
                                   x = column.sel_column(pos);
                                   y =row.sel_row(pos);
                                   panel_lan.Location = new Point(x,y);
                                   break;

                               case 7:     //Gprs
                                   panel_gprs.Enabled = true;
                                     pos++;
                                   x = column.sel_column(pos);
                                   y = row.sel_row(pos);
                                   panel_gprs.Location = new Point(x,y);
                                   break;

                               case 8:     //Wifi
                                   panel_wifi.Enabled = true;
                                   pos++;
                                   x = column.sel_column(pos);
                                   y = row.sel_row(pos);
                                   panel_wifi.Location = new Point(x, y);
                                   break;

                               case 9:     //RF
                                   panel_rf.Enabled = true;
                                   break;

                               case 10:    //Icc
                                   panel_icc.Enabled = true;
                                   break;

                               case 11:    //Msr
                                   panel_msr.Enabled = true;
                                   break;
                           }
                           break;

                       case "N":
                           switch (j)
                           {
                               case 2:     //Printer 
                                   textBox_printer.Text = "No disponible para este modelo";
                                   break;

                               case 3:     //DialUp
                                   textBox_dial.Text = "No disponible para este modelo";
                                   break;

                               case 4:     //UsbH

                                   break;

                               case 5:     //UsbD

                                   break;

                               case 6:     //Lan
                                   textBox_lan.Text = "No disponible para este modelo";
                                   break;

                               case 7:     //Gprs
                                   textBox_gprs.Text = "No disponible para este modelo";
                                   
                               
                                   break;

                               case 8:     //Wifi
                                   textBox_wifi.Text = "No disponible para este modelo";
                                   break;

                               case 9:     //RF
                                   textBox_rf.Text = "No disponible para este modelo";
                                   break;

                               case 10:    //Icc
                                   textBox_icc.Text = "No disponible para este modelo";
                                   break;

                               case 11:    //Msr
                                   textBox_msr.Text = "No disponible para este modelo";
                                   break;
                           }
                           break;
                   }
               }
             
           }



           if (apuntador != "5T4RT" && apuntador != "*")      
            {
               for (int i = 0; i < (search_data.Length - 1); i++)
                {
                    string campo = search_data[i]; //campo: almacena  codigos de los modulos con el separador (#).

                    cadena = campo.TrimStart('#'); //cadena= almacena los codigos de los modulos sin el separador (#).

                    switch (apuntador)
                    {
                        case "8":               //modulo ICC
                            int m_icc = Convert.ToInt32(cadena);
                            m_icc = ejemplo.icc(m_icc);
                            if (test == true)
                            {
                                if(m_icc != 0 )
                                {
                                    m_icc = 6;
                                }
                                else { m_icc = 5; }
                                detail_icc.Enabled = true;
                                printer.prin_ter(m_icc, check_icc, failed_icc, check_msr, failed_msr, failed_rf, check_rf, failed_printer, check_printer, check_keypad, failed_keypad);
                            }
                            
                        break;

                        case "9":               //módulo KEYPAD
                             string m_key =  cadena;
                             int m_key1 = ejemplo.keypad(m_key);
                             
                             
                             if( test == true)
                             {
                                 detail_keypad.Enabled = true;
                                                                 ;
                             }
                             //print_key();
                        break;

                        case "10":               //módulo MSR
                            int m_msr = Convert.ToInt32(cadena);
                            error_r = ejemplo.msr(m_msr);
                            if (test == true)
                            {
                                if (m_msr != 0)
                                {
                                    m_msr = 11;
                                }
                                else {m_msr = 12;}
                                detail_msr.Enabled = true;

                                printer.prin_ter(m_msr,check_icc,failed_icc,check_msr, failed_msr, failed_rf, check_rf, failed_printer, check_printer, check_keypad, failed_keypad);
                            }
                                textBox_msr.Text = error_r;
                        break;

                        case "11":               //módulo PRINTER
                            int m_printer = Convert.ToInt32(cadena);
                            error_r = ejemplo.printer(m_printer);
                            if (test == true)
                            {
                                if (m_printer != 0)
                                {
                                    m_printer = 64;
                                }
                                else { m_printer = 65;  }
                                detail_printer.Enabled = true;
                                printer.prin_ter(m_printer, check_icc, failed_icc, check_msr, failed_msr, failed_rf, check_rf, failed_printer, check_printer, check_keypad, failed_keypad);
                            }
                                textBox_printer.Text = error_r;
                        break;

                        case "12":              //módulo RF
                            int m_rf = Convert.ToInt32(cadena);
                            error_r = ejemplo.rf(m_rf);
                            if (test == true)
                            {
                                if (m_rf != 0)
                                {
                                    m_rf = 17;
                                }else {m_rf = 18;}
                                detail_rf.Enabled = true;
                                printer.prin_ter(m_rf, check_icc, failed_icc, check_msr, failed_msr, failed_rf, check_rf, failed_printer, check_printer, check_keypad, failed_keypad);
                            }
                            textBox_rf.Text = error_r;
                        break;
                    }
                }
            }
       }
       
        //Form Closing
        private void Form_PostTesting_FormClosing(object sender, FormClosingEventArgs e)
        {
            pos_serial.Close();
        }

        //Seleccion Menu - Comunicacion Serial
        private void port_name_SelectedIndexChanged(object sender, EventArgs e)
        {
            button_oppenp.Enabled = true;
        }

        //Opcion Guardar como... - Reportes Menu (Archivo)
        private void item_guardarcomo_Click(object sender, EventArgs e)
        {
            report_pdf.archivo_pdf(save_pdf, textBox_modelo, textBox_serial);
        }

        // Actualizacion parametros portname - COM disponibles.
        private void port_name_MouseClick(object sender, MouseEventArgs e)
        {
            port_name.Items.Clear();
            port.view_com(port_name);
        }

        //Opcion SALIR - Menu (Archivo)
        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pos_serial.Close();
            this.Close();
        }



#endregion

        private void button_testg_Click(object sender, EventArgs e)
        {
            test = true;
        }

        private void textBox_icc_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonsend_uart_Click(object sender, EventArgs e)
        {

        }

        private void buttonrecv_usb_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            textBox_hactual.Text = string.Format("{0:hh:mm:ss}", DateTime.Now);
        }


  

  





        /**
         * print_key() = Muestra el estado de cada tecla del dispositivo y se refleja en el tabPage: "KEYPAD".
         */
        /*public void print_key()
        {
            string key = string.Empty;

            int i;
            for (i = 0; i < ejemplo.array_key.Length; i++)
            {
                key = ejemplo.array_key[i];
                switch (key)
                {
                    case "1":
                        pictureBox21.Visible = true;
                        break;

                    case "2":
                        pictureBox23.Visible = true;
                        break;
                    case "3":
                        pictureBox25.Visible = true;
                        break;
                    case "4":
                        pictureBox27.Visible = true;
                        break;
                    case "5":
                        pictureBox29.Visible = true;
                        break;
                    case "6":
                        pictureBox31.Visible = true;
                        break;
                    case "7":
                        pictureBox33.Visible = true;
                        break;
                    case "8":
                        pictureBox35.Visible = true;
                        break;
                    case "9":
                        pictureBox37.Visible = true;
                        break;
                }
            }
            // Identificando las teclas operativas mediante (✔).
                if(pictureBox27.Visible == false)
                {
                    pictureBox26.Visible = true;
                }
               /* if(pictureBox23.Visible != true)
                {
                    pictureBox22.Visible = true;
                }
                if (pictureBox25.Visible != true)
                {
                    pictureBox24.Visible = true;
                }
                if (pictureBox27.Visible != true)
                {
                    pictureBox26.Visible = true;
                }
                if (pictureBox29.Visible != true)
                {
                    pictureBox28.Visible = true;
                }
                if (pictureBox31.Visible != true)
                {
                    pictureBox30.Visible = true;
                }*/
    }
 

        
        

        
            

            


            

           

           

            
            

           

           

            

           
            

          

            

           
          

            

       

           
         
         

    }



